# Pattern Matching for regular and non-cross patterns
## Requirements
The whole project was written using C++20. Moreover, 2 packages are required

    - C++ boost library (https://www.boost.org/)
    - To use visualisation of the graphs, raylib is required (https://www.raylib.com/)

Both packages most likely can be directly installed using your package manager.
## Using the software
I have provided two variants of the software. Firstly the normal one, which has the
ability to visualise the graph, and another one which has all references to raylib stripped of it, incase installation is not possible.

I will refer to the first of these throughout, the second behaves identically minus the visualisation.

### Flags
The compiled program is found as *main* in the build directory. Multiple flags can be set or are required.

    - -o : set path to output file, this *is required*. CAREFUL! This can and will overwrite non-read protected files.
    - -i : set path to the input file, if not set this will default to a file of multiple non-cross examples
    - -r : if set, the algorithms for regular patterns will be run, if not the algorithms for non-cross patterns will be run
    - -v : if set, the graph which are enumerated will be visualised. Press *escape* to go to the next graph.
    - -e : if set, all solutions will be explicitly enumerated and stored in the outfile. If not, only whether the matching problem is solvable will be stored in the file.
    - -n : if set, the number of solutions is added to the outfile

After the program has finished execution, the output will be written to the out file.

### Format
Check the example input files in the build folder for the input format. It simply consists of "word,pattern" in every line.

### Examples

    - To run the default examples without visualisation, run:  ./main -o out.txt -e -n
    - With visualisation: ./main -o out.txt -e -n -v
    - Custom input-file: ./main -o out.txt -e -n -i in.txt


Here is an example how the visualisation looks:

![Example Visualisation](visExample "Example Visualisation")

