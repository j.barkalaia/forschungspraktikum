#ifndef substringExtensions_H
#define substringExtensions_H
#include<vector>
#include<string>
std::vector<std::vector<int>> calcRR(std::string w, std::string v);
std::vector<std::vector<std::vector<int>>> calcR_efficient(std::vector<std::pair<int, int>> variables, std::vector<std::vector<std::pair<int, int>>> varDecomposed, std::string w, std::string pat);
#endif
