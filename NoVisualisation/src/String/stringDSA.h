#ifndef stringDSA_H
#define stringDSA_H
#include <vector>
#include <string>
struct lcpStructures {
    std::vector<std::vector<int>> st;
    std::vector<int> invertedSA;
    std::vector<int> LCP;
};
std::vector<std::vector<int>> calcDBF(std::string w);
std::vector<std::vector<int>> calcSparseTableMin(std::vector<int>LCP);
int LCPP(lcpStructures, unsigned int x, unsigned int y);
int findPeriod(std::string t);
bool isPrimitive(std::string t);
int LCP(std::string w, int i, int j);
int LCS(std::string w, int i, int j);
std::pair<std::vector<int>, int> findAllOccurences(std::string p, std::string s, int searchPos);
#endif
