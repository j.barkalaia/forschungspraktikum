#ifndef Primitivity_H
#define Primitivity_H
#include <boost/container_hash/hash.hpp>
#include<unordered_map>
#include<vector>
int succInProgression(std::tuple<int, int, int> prog, int i);
int predInProgression(std::tuple<int, int, int> prog, int i);
int SUCC(int i, std::pair<int, int> v, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
int PRED(int i, std::pair<int, int> v, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> constructH(std::vector<std::vector<int>> DBF);
std::tuple<int, int, int> Occ(std::pair<int, int> x, std::pair<int, int> y, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
std::tuple<int, int, int> LargePS(std::pair<int, int> x, std::pair<int, int> y, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
std::tuple<int, int, int> bordersLarger(std::pair<int, int> u, int M, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
bool isPrim(std::pair<int, int> substr, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
bool isPrimAlt(std::pair<int, int> substr, std::unordered_map<std::tuple<int, int, int>, std::tuple<int, int, int>, boost::hash<std::tuple<int, int, int>>> H, std::vector<std::vector<int>> DBF);
bool elementOfProgression(std::tuple<int, int, int> prog, int i);
#endif
