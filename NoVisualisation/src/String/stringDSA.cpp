#include<vector>
#include<string>
#include<algorithm>
#include "stringDSA.h"
using namespace std;
/*
This file contains various methods for computing simple and common string data-structures.
*/


/*
Builds the Dictionary of Basic Factors for the string w
Runtime: O(n)
*/
vector<vector<int>> calcDBF(string w) {
    unsigned int n = w.length();
    vector<vector<int>> DBF(__bit_width(n), vector<int>(n));
    for(int i = 0; i < n; i++) {
        DBF[0][i] = w[i] - 'a' + 1;
    }
    for(int i = 0; i < n; i++) {
        pair<int, pair<int, int>> posPair;
        if(DBF[0][i] == DBF[0][i+1]) {
            posPair.first = 2;
            posPair.second = make_pair(i, i+1);
        }
        else {
            posPair.first = 1;
            posPair.second = make_pair(i, -1);
        }
    }
    int pow = 1;
    for(int j = 1; j < __bit_width(n); j++) {
        pow *= 2;
        vector<pair<pair<int, int>, int>> temp;
        for(int i = 0; i < n - pow + 1; i++) {
            temp.push_back(make_pair(make_pair(DBF[j-1][i], DBF[j-1][(pow >> 1) + i]), i));
        }

        sort(temp.begin(), temp.end());

        int rank = 1;

        for(int i = 0; i < n - pow + 1; i++) {
            pair<pair<int, int>, int> curr = temp[i];
            DBF[j][curr.second] = rank;
            if(i < n - pow && curr.first != temp[i+1].first) rank++;
        }
    }
    for(int i = 0; i < DBF.size(); i++) {
        while(*(DBF[i].end() - 1) == 0) DBF[i].pop_back();
    }
    return DBF;
}

/**
Builds the sparse table used for constant time minimum queries on the LCP array
*/
vector<vector<int>> calcSparseTableMin(vector<int>LCP) {
    unsigned int N = LCP.size();
    vector<vector<int>> st(__bit_width(N) + 2, vector<int>(N));
    st[0] = LCP;
    for (int i = 1; i <= (__bit_width(N) + 1); i++) {
        for (int j = 0; j + (1 << i) <= N; j++)
            st[i][j] = min(st[i - 1][j], st[i - 1][j + (1 << (i - 1))]);
    }
    return st;
}


/**
Returns the LCP of the substrings beginning at positions x and y
Runtime: constant
*/

int LCPP(lcpStructures s, unsigned int x, unsigned int y) {
    vector<vector<int>> st = s.st;
    vector<int> invertedSA = s.invertedSA;
    vector<int> LCP = s.LCP;
    if(x == y) return LCP.size() - 1 - x;
    x = invertedSA[x];
    y = invertedSA[y];
    if(x > y) swap(x, y);
    x += 1;
    int i = __bit_width(y - x + 1) - 1;
    int minimum = min(st[i][x], st[i][y - (1 << i) + 1]);
    int res = LCP[minimum];
    return minimum;
}

/*
Find the period of a string using z-function
Runtime: linear
*/
int findPeriod(string t) {
	int n=t.size();
	vector<int>z(n,0);
	for (int i=1,l=0,r=0;i<n;++i){
		if (i<=r) z[i]=min(r-i+1,z[i-l]);
		while (i+z[i]<n && t[z[i]]==t[z[i]+i]) ++z[i];
		if (z[i]+i-1>r){
			r=z[i]+i-1;
			l=i;
		}
		if (z[i]+i==n) return i;
	}
	return n;
}

/*
Naive implementation, for testing!
*/
bool isPrimitive(string t) {
    const int n = t.length();
    for(int len = 1; len <= n / 2; len++) {
        int power = n / len;
        if(power * len == n) {
            string res = "";
            for(int i = 0; i < power; i++) res += t.substr(0, len);
            if(res == t) return false;
        }
    }
    return true;
}

/*
Naive implementation, just for testing!
*/
int LCP(string w, int i, int j) {
    int res = 0;
    if(i < 0 || j < 0) return 0;
    while(w[i+res] == w[j+res] && i + res < w.length() && j + res < w.length()) res++;
    return res;
}

/**
Naive Longest Common Suffix
*/
int LCS(string w, int i, int j) {
    int res = 0;
    if(i < 0 || j < 0) return 0;
    while(w[i-res] == w[j-res] && i-res >= 0 && j - res >= 0) {
        res++;
    }
    return res;
}

/*
Find all occurences of s in p
*/
pair<vector<int>, int> findAllOccurences(string p, string s, int searchPos) {
    vector<int> positions;
    size_t pos = p.find(s, searchPos);
    size_t first = pos;
    while(pos != string::npos) {
        positions.push_back(pos);
        pos = p.find(s, pos+1);
    }
    return make_pair(positions, pos);
}
