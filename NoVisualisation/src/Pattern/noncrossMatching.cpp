#include<optional>
#include<list>
#include<algorithm>
#include <stdexcept>
#include "../String/SuffixArray/Skew.h"
#include "patternDSA.h"
#include "../String/Crochemore.h"
#include "../String/stringDSA.h"
#include "../String/substringExtensions.h"
#include<boost/iterator/zip_iterator.hpp>
#include "../String/Primitivity.h"

using namespace std;
typedef tuple<int, int, int, pair<int, int>> doubleSideProgressionEdge; //starting node, period, end, offset
typedef tuple<int, int, int, optional< pair< int, int > >  > arithmProg;
int numSolutionsNC(vector<vector<int>> nodes, vector<vector<vector<arithmProg>>> edges, vector<vector<int>> progressionIndices, vector<list<doubleSideProgressionEdge>> progressions, vector<pair<int, int>> variables);


/*
Calculate nextBranch for the given graph. See thesis text for details.
In short nextBranch gives for a node the nearest node to it, which has multiple outgoing edges.
*/
vector<vector<pair<int, int>>> walkNextNode(vector<vector<int>> nodes, vector<vector<vector<arithmProg>>> edges, vector<vector<int>> progressionIndices) {
    vector<vector<pair<int, int>>> nextBranch = vector<vector<pair<int, int>>>(nodes.size());
    nextBranch[nodes.size()-1] = vector<pair<int, int>>((nodes.back()).size(), make_pair(-1, -1));
    for(int i = nodes.size() - 2; i >= 0; i--) {
        for(int j = 0; j < nodes[i].size(); j++) {
            if(edges[i][j].size() > 1) nextBranch[i].push_back(make_pair(i, j));
            else {
                //We need to check whether the only edge contained is periodic and, if yes, points to multiple nodes
                int edgePeriod = get<1>(edges[i][j][0]);
                if((edgePeriod == 1 && get<0>(edges[i][j][0]) + 1 < nodes[i+1].size())) nextBranch[i].push_back(make_pair(i, j));
                else nextBranch[i].push_back(nextBranch[i+1][get<0>(edges[i][j][0])]);
                if(edgePeriod > 1) {
                    auto [progIndex, progPosIndex] = get<3>(edges[i][j][0]).value();
                    vector<int> currProg = progressionIndices[progIndex];

                    if(currProg.size() - 1 <= progPosIndex) nextBranch[i].push_back(nextBranch[i+1][get<0>(edges[i][j][0])]);
                    else nextBranch[i].push_back(make_pair(i, j));
                }
                if(edgePeriod == -1) nextBranch[i].push_back(nextBranch[i+1][get<0>(edges[i][j][0])]);
            }
        }
    }
    return nextBranch;
}

/*
Helper function - simply invert all edges.
*/
vector<vector<vector<arithmProg>>> invertEdges(vector<vector<vector<arithmProg>>> edges) {
    vector<vector<vector<arithmProg>>> invertedEdges(edges.size(), vector<vector<arithmProg>>(edges[0].size()));
    for(int i = 0; i < edges.size() - 1; i++) {
        for(int j = 0; j < edges[i].size(); j++) {
            for(arithmProg edge : edges[i][j]) {
                invertedEdges[i+1][get<0>(edge)].push_back(arithmProg{j, get<1>(edge), get<2>(edge), get<3>(edge)});
            }
        }
    }
    return invertedEdges;
}




/*
Main Method for turning the graph returned by the non-cross matching algorithm into a graph which only contains edges and nodes which are part of atleast one solution.
Iterates reversely through the edges, and checks for each whether it leads to a node which still exists. If no, the edge is removed. If a node remains with no outgoing edges it is removed.
*/
tuple<vector<vector<int>>, vector<vector<vector<arithmProg>>>, vector<vector<int>>> cleanGraph(vector<vector<int>> nodes, vector<vector<vector<arithmProg>>> edges, vector<pair<int, int>> variables, vector<list<doubleSideProgressionEdge>> progressions, vector<vector<bool>> matched) {
    const int n = edges[0].size();

    vector<vector<vector<arithmProg>>> invertedEdges = invertEdges(edges);
    vector<vector<vector<arithmProg>>> edgeCp(edges.size(), vector<vector<arithmProg>>(edges[0].size()));
    vector<vector<int>> finalMatchablePositions(nodes.size());
    finalMatchablePositions[nodes.size()-1].push_back((nodes.back()).back());
    vector<vector<vector<arithmProg>>> finalEdges(edges.size() - 1);
    vector<bool> finalMatchedPrev(n, false);
    finalMatchedPrev[n-1] = true;

    vector<vector<int>> progressionIndices;


    for(int i = nodes.size() - 2; i >= 0; i--) {
        vector<bool> finalMatched(n, false);
        sort(nodes[i].begin(), nodes[i].end());
        if(variables[i].second == 1) {
            int ptr = 0;
            for(int j : nodes[i]) {
                for(arithmProg e : edges[i][j]) { //Since we are in the case a_i = X each position contains maximally one edge e
                    //Since the nodes are in ascending order we can just use one pointer
                    while(finalMatchablePositions[i+1][ptr] < get<0>(e) && ptr < (finalMatchablePositions[i+1].size() - 1)) ptr++;
                    if(finalMatchablePositions[i+1][ptr] >= get<0>(e)) {
                        finalMatchablePositions[i].push_back(j);
                        finalEdges[i].push_back(vector<arithmProg>{arithmProg{ptr, 1, n, std::nullopt}});
                    }
                    else break;
                }
            }
        }
        else {
            /*
           The complicated part. We stored the double-sided progressions during the algorithm and now check, which positions of these are still contained.
           Check thesis for details.
            */
            for(doubleSideProgressionEdge e : progressions[i]) {
                int period = get<1>(e);
                int end = get<2>(e);
                auto [offsetL, offsetR] = get<3>(e);
                int rPtr = 0;
                int lPtr = get<0>(e) - offsetR;

                progressionIndices.push_back({});

                while(lPtr < n) {

                    while((!elementOfProgression({get<0>(e), period, end}, finalMatchablePositions[i+1][rPtr])) && rPtr < (finalMatchablePositions[i+1].size() - 1) && finalMatchablePositions[i+1][rPtr] <= end) rPtr++;

                    if(elementOfProgression({get<0>(e), period, end}, finalMatchablePositions[i+1][rPtr])) {
                        if((progressionIndices.back()).empty() || (progressionIndices.back()).back() != rPtr) (progressionIndices.back()).push_back(rPtr);


                        while(lPtr + offsetR <= finalMatchablePositions[i+1][rPtr]) {
                            if(matched[2*i][lPtr]) {
                                int cNextElement = 1;
                                if(lPtr + offsetL == finalMatchablePositions[i+1][rPtr]) cNextElement = 0; //Special case if we already have a singular edge in place
                                edgeCp[i][lPtr].push_back(arithmProg{rPtr, period, -1,  make_pair(progressionIndices.size() - 1, (progressionIndices.back()).size() - cNextElement)}); //EndIndex -1 entfernen allg wahrscheinlich
                                finalMatched[lPtr] = true;
                            }
                            lPtr += period;
                        }
                    }
                    else break;
                    if(rPtr < (finalMatchablePositions[i+1].size() - 1)) rPtr++;
                    else break;
                }
            }

            for(int j = 0; j < finalMatchablePositions[i+1].size(); j++) {
                int k = finalMatchablePositions[i+1][j];
                for(arithmProg e : invertedEdges[i+1][k]) {
                    finalMatched[get<0>(e)] = true;
                    edgeCp[i][get<0>(e)].push_back(arithmProg{j, -1, j, std::nullopt});
                }
            }
            finalMatchedPrev = finalMatched;
            for(int j = 0; j < n; j++) {
                if(finalMatched[j]) {
                    finalMatchablePositions[i].push_back(j);
                    finalEdges[i].push_back(edgeCp[i][j]);
                }
            }
        }
    }

    return tuple<vector<vector<int>>, vector<vector<vector<arithmProg>>>, vector<vector<int>>>{finalMatchablePositions, finalEdges, progressionIndices};
}

/*
Main Algorithm for generating the graph G' containing all the solutions to the pattern matching problem for non-cross patterns.
This is described in detail in the thesis.
*/
pair<tuple<vector<vector<int>>, vector<vector<vector<arithmProg>>>, vector<vector<int>>>, int> nonCrossGraph(string w, string pat) {
    const int n = w.length();
    const int m = pat.length();


    Skew SA;
    SA.addString(w+pat);
    SA.makeSuffixArray();
    struct lcpStructures lcpStruct;
    lcpStruct.LCP =(SA.makeLCPArray()).first;
    lcpStruct.invertedSA = (SA.makeLCPArray()).second;
    lcpStruct.st = calcSparseTableMin(lcpStruct.LCP);

    auto [variables, words] = factorisePattern(pat);
    const int s = variables.size();
    vector<vector<pair<int, int>>> varDecomposed = decomposeVariableBlocks(pat, variables);

    vector<vector<int>> P = crochemoreP(w);
    vector<vector<vector<int>>> R = calcR(variables, varDecomposed, w, pat);

    vector<vector<vector<arithmProg>>> edges(variables.size()+ 1, vector<vector<arithmProg>> (n+1));
    vector<vector<int>> nodes(variables.size() + 1);
    vector<vector<int>> matchablePositions(variables.size() + words.size() + 1);
    vector<vector<bool>> matched(variables.size()+words.size()+1, vector<bool>(n+1));
    matched[0][0] = true;
    matchablePositions[0].push_back(0);
    nodes[0].push_back(0);
    vector<vector<vector< optional< list<doubleSideProgressionEdge>::iterator > >>> powerMapping(variables.size()+words.size() + 1, vector<vector< optional< list<doubleSideProgressionEdge>::iterator > >>(n+2, vector< optional< list<doubleSideProgressionEdge>::iterator > >(n+1, std::nullopt)));
    vector<list<doubleSideProgressionEdge>> progressions(variables.size()+1);

    //MAIN-LOOP: Iterate through every block
    for(int i = 0; i < variables.size() + words.size(); i++) {
        //1. Match new constant word
        for(int j : matchablePositions[i]) { //For all matchings, try to extend them via an LCP query
            if(LCPP(lcpStruct, j, n + words[i/2].first) >= words[i/2].second) {
                matched[i+1][j+words[i/2].second] = true;
                matchablePositions[i+1].push_back(j + words[i/2].second);
            }
        }

        i++;//Now we match variables
        if(i >= variables.size() + words.size()) break;

        //CASE a_m = x_m
        if(variables[(i-1)/2].second == 1) {
            //Simply find the minimum and insert all positions greater than it
            int minmatch = *min_element(matchablePositions[i].begin(), matchablePositions[i].end());
            for(int a = minmatch+1; a <= n; a++) { //We can match every position after the minimum position we can match with a_i-1
                matchablePositions[i+1].push_back(a);
                matched[i+1][a] = true;
            }
            for(int p : matchablePositions[i]) { //Every matchable position of a_i-1 can match every position after that for a_i
                if(p + words[(i-1)/2].second < n) edges[(i-1)/2][p-words[(i-1)/2].second].push_back(arithmProg{p+1, 1, n, std::nullopt});
            }

        }

        //CASE a_i = a_i' xvx
        else {
            //CASE v_i v primitive -> check all Elements of the sets R
            if(varDecomposed[i/2].back().second == 1) {
                for(int j = 1; j <= n; j++) {
                    for(int t : R[i/2][j]) {
                        int matchlen = checkSubstitutionSuffix(pat, w, j, {{j-t, t, 1}}, varDecomposed[i/2]);
                        if (matchlen != -1) {
                            if(matched[i][j-matchlen]) {
                                edges[(i-1)/2][j - matchlen - words[(i-1)/2].second].push_back(arithmProg(j, -1, j, std::nullopt));
                                matchablePositions[i+1].push_back(j);
                                matched[i+1][j] = true;
                            }
                        }
                    }
                }
            }
            //CASE v_i v not primitive - Basic Assignments
            int e = 0; //Contains the number of variables in the current block
            for(int k = 0; k  < varDecomposed[i/2].size(); k += 2) e += varDecomposed[i/2][k].second;
            for(int j = 1; j <= n; j++) {
                for(int d = 0; d < P[j].size(); d++) {
                    pair<int, int> t = {j - P[j][d], P[j][d]};
                    pair<int, int> v = (varDecomposed[i/2].back().second == 1 ? (*(varDecomposed[i/2].end() - 2)) : pair<int,int>({0, 0}));
                    int l = v.second / t.second;
                    if(v.second > 0 && (LCPP(lcpStruct, v.first+n, t.first) < t.second || LCPP(lcpStruct, v.first+n, v.first+n+t.second) < v.second - t.second)) continue; //Does v have the form t^j t''
                    pair<int, int> ts = {n + v.first + v.second - (v.second -l * t.second), v.second -l * t.second}; //Pos of t' w+pat
                    pair<int, int> vi;
                    vi.second = ((l+1) * t.second) - v.second; // v^-1 t^l+1 = v_i
                    vi.first = n + v.first + t.second - vi.second; //Pos in w+pat
                    if(v.second == 0) vi.first = t.first;
                    int matchlen = checkSubstitutionSuffix(pat, w, j, {{vi.first, vi.second, 1}}, varDecomposed[i/2]);
                    if (matchlen != -1) {
                        if(matched[i][j-matchlen]) {
                            edges[(i-1)/2][j - matchlen - words[(i-1)/2].second].push_back(arithmProg(j, -1, j, std::nullopt));
                            if(matchablePositions[i+1].empty() || *(--matchablePositions[i+1].end()) != j) matchablePositions[i+1].push_back(j);
                            matched[i+1][j] = true;
                        }
                    }
                    const int b = checkVariableBlockPeriodic(varDecomposed[i/2], t, ts, v, n, lcpStruct);
                    if(b < varDecomposed[i/2].size()) {
                        //Block is not |t|-periodic, hence we have a unique matching possibility
                        int matchlen = uniquePeriodicMatching(b, varDecomposed[i/2], t, ts, j, v, vi, n, lcpStruct, w, pat);
                        if(matchlen != -1) {
                            if(matched[i][j-matchlen]) { //Ist der Rest Matchbar
                                edges[(i-1)/2][j - matchlen - words[(i-1)/2].second].push_back(arithmProg(j, -1, j, std::nullopt));
                                if(matchablePositions[i+1].empty() || *(matchablePositions[i+1].end() - 1) != j) matchablePositions[i+1].push_back(j);
                                matched[i+1][j] = true;
                            }
                        }
                    }
                    else {
                        //Block is |t|-periodic, hence find basic assignments to be extended later
                        int matchlen = checkSubstitutionSuffix(pat, w, j, {{vi.first, vi.second, 1}, {t.first, t.second, 1}},varDecomposed[i/2]); //vi + t
                        if(matchlen != -1) {
                            if(matched[i][j-matchlen]) {
                                if(matchablePositions[i+1].empty() || *(matchablePositions[i+1].end() - 1) != j) matchablePositions[i+1].push_back(j);

                                doubleSideProgressionEdge newProgression = {j, e*t.second, j, make_pair(matchlen + words[(i-1)/2].second - e*t.second, matchlen + words[(i-1)/2].second)};
                                //We store each progression seperately for ease of access later.
                                //Remember: These progressions are "double-sided" ie. the progression is both for the parent and the child nodes
                                //We store in last position in the progressions the offset between the two arithmetic progressions (which of course have identical periods)
                                progressions[(i-1)/2].push_front(newProgression);
                                powerMapping[i+1][j][d] = progressions[(i-1)/2].begin();
                                matched[i+1][j] = true;
                            }
                        }
                    }
                }
            }
            //Extend basic assignments
            for(int j = 1; j <= n; j++) {
                for(int d = 0; d < P[j].size(); d++) {
                    int t = P[j][d];
                    if(j < e*t || P[j-e*t].size() <= d) continue;
                    if(P[j-e*t][d] != t || LCPP(lcpStruct, j-e*t-t, j-t) < t) continue; //Check[j-e*t_curr][d] = t_curr
                    if(!powerMapping[i+1][j-e*t][d].has_value()) continue; //Is the previous part matchable with a P[d] power
                    if(LCPP(lcpStruct, j-e*t, j-e*t + t) < e*t-t) continue; //|t|-periodicity of w[j-e|t|..j]

                    if(powerMapping[i+1][j][d].has_value()) { //In this case we have already added a progression here for the basic assignments - remove it, it is now part of the extended progression
                        progressions[(i-1)/2].erase(powerMapping[i+1][j][d].value());
                    }

                    list<doubleSideProgressionEdge>::iterator currentProgression =  powerMapping[i+1][j-e*t][d].value();
                    powerMapping[i+1][j][d] = currentProgression;

                    get<2>(*currentProgression) += e*t;
                    if(!matched[i+1][j]) matchablePositions[i+1].push_back(j);
                    matched[i+1][j] = true;
                }
            }
        }
        nodes[(i-1)/2 + 1] = matchablePositions[i+1];
    }

    if(matchablePositions.back().empty() || matchablePositions.back().back() != n) {
        throw std::runtime_error("not matchable");
    }
    auto cleaned = cleanGraph(nodes, edges, variables, progressions, matched);
    return make_pair(cleaned, numSolutionsNC(get<0>(cleaned), get<1>(cleaned), get<2>(cleaned), progressions, variables));
}

/*
The decision algorithm for non-cross patterns. Extremely similar to the nonCrossGraph algorithm, hence see there for comments and details.
*/
bool nonCrossDecision(string w, string pat) {
    const int n = w.length();
    const int m = pat.length();

    Skew SA;
    SA.addString(w+pat);
    SA.makeSuffixArray();
    struct lcpStructures lcpStruct;
    lcpStruct.LCP =(SA.makeLCPArray()).first;
    lcpStruct.invertedSA = (SA.makeLCPArray()).second;
    lcpStruct.st = calcSparseTableMin(lcpStruct.LCP);

    auto [variables, words] = factorisePattern(pat);
    const int s = variables.size();
    vector<vector<pair<int, int>>> varDecomposed = decomposeVariableBlocks(pat, variables);

    vector<vector<int>> P = constructP(w);
    vector<vector<vector<int>>> R = calcR(variables, varDecomposed, w, pat);

    vector<vector<int>> matchablePositions(variables.size() + words.size() + 1);
    vector<vector<bool>> matched(variables.size()+words.size()+1, vector<bool>(n+1));
    matched[0][0] = true;
    matchablePositions[0].push_back(0);
    vector<vector<vector<bool>>> powerMapping(variables.size()+words.size() + 1, vector<vector<bool>>(n+2, vector<bool>(n+1, false)));

    for(int i = 0; i < variables.size() + words.size(); i++) { //Main-Loop
        //1. Match new constant v_i
        if(matchablePositions[i].empty()) return false;
        for(int j : matchablePositions[i]) { //For all matchings, try to extend them via an LCP query
            if(LCPP(lcpStruct, j, n + words[i/2].first) >= words[i/2].second) {
                matched[i+1][j+words[i/2].second] = true;
                matchablePositions[i+1].push_back(j + words[i/2].second);
            }
        }
        if(matchablePositions[i+1].empty()) return false;

        i++;//Now we match variables
        if(i >= variables.size() + words.size()) break;

        //CASE a_m = x_m
        if(variables[(i-1)/2].second == 1) {
            //Simply find the minimum and insert all positions greater than it
            int minmatch = *min_element(matchablePositions[i].begin(), matchablePositions[i].end());
            for(int a = minmatch+1; a <= n; a++) { //We can match every position after the minimum position we can match with a_i-1
                matchablePositions[i+1].push_back(a);
                matched[i+1][a] = true;
            }
        }

        //CASE a_i = a_i' xvx
        else {
            //CASE v_i v primitive
            if(varDecomposed[i/2].back().second == 1) {
                for(int j = 1; j <= n; j++) { //O(n)
                    for(int t : R[i/2][j]) {  //log(n)
                        int matchlen = checkSubstitutionSuffix(pat, w, j, {{j-t, t, 1}}, varDecomposed[i/2]);
                        if (matchlen != -1) {
                            if(matched[i][j-matchlen]) {
                                matchablePositions[i+1].push_back(j);
                                matched[i+1][j] = true;
                            }
                        }
                    }
                }
            }
            //CASE v_i v not primitive - Basic Assignments
            int e = 0;
            for(int k = 0; k  < varDecomposed[i/2].size(); k += 2) e += varDecomposed[i/2][k].second;

            for(int j = 1; j <= n; j++) {
                for(int d = 0; d < P[j].size(); d++) {
                    pair<int, int> t = {j - P[j][d], P[j][d]};
                    pair<int, int> v = (varDecomposed[i/2].back().second == 1 ? (*(varDecomposed[i/2].end() - 2)) : pair<int,int>({0, 0}));
                    int l = v.second / t.second;
                    if(v.second > 0 && (LCPP(lcpStruct, v.first+n, t.first) < t.second || LCPP(lcpStruct, v.first+n, v.first+n+t.second) < v.second - t.second)) continue;
                    pair<int, int> ts = {n + v.first + v.second - (v.second -l * t.second), v.second -l * t.second}; //t' in w+pat
                    pair<int, int> vi;
                    vi.second = ((l+1) * t.second) - v.second; // v^-1 t^l+1 = v_i
                    vi.first = n + v.first + t.second - vi.second; //Pos in w+pat
                    if(v.second == 0) vi.first = t.first;
                    int matchlen = checkSubstitutionSuffix(pat, w, j, {{vi.first, vi.second, 1}}, varDecomposed[i/2]);
                    if (matchlen != -1) {
                        if(matched[i][j-matchlen]) {
                            if(matchablePositions[i+1].empty() || *(--matchablePositions[i+1].end()) != j) matchablePositions[i+1].push_back(j);
                            matched[i+1][j] = true;
                        }
                    }
                    const int b = checkVariableBlockPeriodic(varDecomposed[i/2], t, ts, v, n, lcpStruct);
                    if(b < varDecomposed[i/2].size()) {
                        int matchlen = uniquePeriodicMatching(b, varDecomposed[i/2], t, ts, j, v, vi, n, lcpStruct, w, pat);
                        if(matchlen != -1) {
                            if(matched[i][j-matchlen]) {
                                if(matchablePositions[i+1].empty() || *(matchablePositions[i+1].end() - 1) != j) matchablePositions[i+1].push_back(j);
                                matched[i+1][j] = true;
                            }
                        }
                    }
                    else {
                        //h_vi(a_i) |t| periodic
                        int matchlen = checkSubstitutionSuffix(pat, w, j, {{vi.first, vi.second, 1}, {t.first, t.second, 1}},varDecomposed[i/2]); //vi + t
                        if(matchlen != -1) {
                            if(matched[i][j-matchlen]) {
                                if(matchablePositions[i+1].empty() || *(matchablePositions[i+1].end() - 1) != j) matchablePositions[i+1].push_back(j);
                                powerMapping[i+1][j][d] = true;
                                matched[i+1][j] = true;
                            }
                        }
                    }
                }
            }
            //Extend basic assignments
            for(int j = 1; j <= n; j++) {
                for(int d = 0; d < P[j].size(); d++) {
                    int t = P[j][d];
                    if(j < e*t || P[j-e*t].size() <= d) continue;
                    if(P[j-e*t][d] != t || LCPP(lcpStruct, j-e*t-t, j-t) < t) continue; //Check[j-e*t_curr][d] = t_curr
                    if(!powerMapping[i+1][j-e*t][d]) continue; //Is the previous part matchable with a P[d] power
                    if(LCPP(lcpStruct, j-e*t, j-e*t + t) < e*t-t) continue; //|t|-periodicity of w[j-e|t|..j]

                    powerMapping[i+1][j][d] = true;
                    if(!matched[i+1][j]) matchablePositions[i+1].push_back(j);
                    matched[i+1][j] = true;
                }
            }
        }
    }

    if(matchablePositions.back().empty() || matchablePositions.back().back() != n) return false;
    else return true;
}

/*
Given a graph G of the non-cross matching problem instance, return the number of paths of maximal length i.e. the number of solutions.
Pretty straight forward, for periodic edges use pointers to stay in desired run-time complexity.
*/
int numSolutionsNC(vector<vector<int>> nodes, vector<vector<vector<arithmProg>>> edges, vector<vector<int>> progressionIndices, vector<list<doubleSideProgressionEdge>> progressions, vector<pair<int, int>> variables) {
    vector<vector<int>> numSolNode = nodes;
    for(int i = 0; i < nodes.size(); i++) {
        for(int j = 0; j < nodes[i].size(); j++) numSolNode[i][j] = 0;
    }
    for(int i = 0; i < numSolNode.back().size(); i++)  numSolNode.back()[i] = 1;
    int k = 0;
    for(int i = numSolNode.size() - 2; i >= 0; i--) {
        if(variables[i].second == 1) {
            int sumPtr = nodes[i+1].size()-1;
            int resPtr = nodes[i].size()-1;
            int currSum = numSolNode[i+1][sumPtr];
            while(resPtr >= 0) {
                while(sumPtr > get<0>(edges[i][resPtr][0])) {
                    sumPtr--;
                    currSum += numSolNode[i+1][sumPtr];
                }
                numSolNode[i][resPtr] = currSum;
                resPtr--;
            }
        }
        else {
            for(int j = 0; j < nodes[i].size(); j++) {
                for(arithmProg e : edges[i][j]) {
                    auto [start, period, end, r] = e;
                    if(period == -1) numSolNode[i][j] += numSolNode[i+1][start];
                }
            }

            for(doubleSideProgressionEdge e : progressions[i]) {
                auto [start, period, end, offset] = e;
                int offsetR = offset.second;
                int sumPtr = progressionIndices[k].size() - 1;
                int lPtr = nodes[i].size() - 1;
                int currSum = 0;
                while(sumPtr >= 0 || lPtr >= 0) {
                    while(!elementOfProgression({start-offsetR, period, end-offsetR}, nodes[i][lPtr]) && lPtr > 0 && nodes[i][lPtr] >= start) lPtr--;
                    if(elementOfProgression({start-offsetR, period, end-offsetR}, nodes[i][lPtr])) {
                        while(sumPtr >= 0 && nodes[i][lPtr] + offsetR <= nodes[i+1][progressionIndices[k][sumPtr]]) {
                            currSum += numSolNode[i+1][progressionIndices[k][sumPtr]];
                            sumPtr--;
                        }
                        numSolNode[i][lPtr] += currSum;
                        lPtr--;
                    }
                    else break;
                }
                k++;
            }
        }
    }
    long long finalSum = 0;
    for(int s : numSolNode[0]) finalSum += s;
    return finalSum;
}
