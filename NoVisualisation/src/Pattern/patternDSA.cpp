#include<vector>
#include<string>
using namespace std;
#include"../String/stringDSA.h"


/*
This file contains various simple, auxiliary methods used in the main algorithms.
*/


/*
Factorise pattern as a sequence of ranges, where each range represents a variable or word block alternately.
*/
pair<vector<pair<int, int>>, vector<pair<int, int>>> factorisePattern(string pat) {
    vector<pair<int, int>> variables;
    vector<pair<int, int>> words;
    int lastVarPosition = -1;
    int firstVarPosition = 0;
    char currvar = ' ';
    char currchar = ' ';
    for(int i=0;i<pat.length();i++) {
        currchar = pat[i];
        if(isupper(pat[i])) { //Variable gefunden
            if(currvar != pat[i]) { //Neuer Variablen Block gefunden
                variables.push_back(make_pair(firstVarPosition, lastVarPosition - firstVarPosition + 1));
                words.push_back(make_pair(lastVarPosition+1, i-lastVarPosition-1));
                //Update für neuen Variablenblock
                currvar = pat[i];
                firstVarPosition = i;
            }
            lastVarPosition = i;
        }
    }
    variables.push_back(make_pair(firstVarPosition, lastVarPosition - firstVarPosition + 1));
    if(pat.length() > lastVarPosition+1) words.push_back(make_pair(lastVarPosition+1, pat.length()-lastVarPosition-1));
    variables.erase(variables.begin());
    return make_pair(variables, words);
}

pair<vector<int>, vector<string>> factoriseNaive(string pat) {
    vector<int> variables;
    vector<string> words;
    int j = -1;
    for(int i=0;i<pat.length();i++) {
        if(isupper(pat[i])) {
            variables.push_back(i);
            words.push_back(pat.substr(j+1, i-j-1));
            j=i;
        }
    }
    words.push_back(pat.substr(j+1, pat.length()-j-1));
    return make_pair(variables, words);
}

/*
Find the R sets as detailed in the paper.
*/
vector<vector<int>> generateR(string w, string v) {
    const int n = w.length();
    const int m = v.length();

    size_t pos = w.find(v, 0);
    vector<vector<int>> R(n+1);
    while(pos != string::npos)
    {
        for(int len = 1; len <= pos; len++) {
            if(w.substr(pos-len, len) == w.substr(pos+m, len)) {
                string uv = w.substr(pos+m, len) + v;
                if(isPrimitive(uv)) R[pos+m+len].push_back(len);
            }
        }
        pos = w.find(v,pos+1);
    }
    return R;
}

/*
Factorize a singular variable block in ranges each representing either a (constant) string or a number of adjacent variables.
*/
vector<vector<pair<int, int>>> decomposeVariableBlocks(string pat, vector<pair<int, int>> variables) {
    const int s = variables.size();
    vector<vector<pair<int, int>>> varDecomposed(s);
    for(int i=0;i<s;i++) {
        int lastLen = 0;
        bool varRepetition = false;
        for(int j = 0; j < variables[i].second; j++) {
            bool isVariable = isupper(pat[variables[i].first + j]);
            if(isVariable && !varRepetition) {
                if(j-lastLen != 0) varDecomposed[i].push_back(make_pair(variables[i].first + lastLen, j-lastLen));
                lastLen = j;
                varRepetition = true;
            }
            else if(!isVariable && varRepetition) {
                varDecomposed[i].push_back(make_pair(variables[i].first + lastLen, j - lastLen));
                lastLen = j;
                varRepetition = false;
            }
        }
        varDecomposed[i].push_back(make_pair(variables[i].first + lastLen, variables[i].second-lastLen));
    }
    return varDecomposed;
}


vector<vector<vector<int>>> calcR(vector<pair<int, int>> variables, vector<vector<pair<int, int>>> varDecomposed, string w, string pat) {
    const int s = variables.size();
    const int n = w.length();
    vector<vector<vector<int>>> R(s, vector<vector<int>>(n+1));
    int i = 0;
    for(vector<pair<int, int>> var : varDecomposed) {
        if(var.size() > 1) {
            pair<int, int> w_pi = *(var.end() - 2);
            R[i] = generateR(w, pat.substr(w_pi.first, w_pi.second));
        }
        i++;
    }
    return R;
}

/*
Naive implementation, just for testing!
*/
vector<vector<int>> constructP(string w) {
    const int n = w.length();
    vector<vector<int>> P(n+1);
    for(int i = 1; i <= n; i++) {
        for(int len = 1; len <= i /2; len++) {
            int period = findPeriod(w.substr(i-len, len));
            if(period <= len/2 && (len% period) == 0) continue;
            if(w.substr(i-len, len) == w.substr(i-2*len, len)) {
                //cerr << "found: " << len << endl;
                P[i].push_back(len);
            }
        }
    }
    return P;
}


/*
Checks whether substituting the substrings q[a1..a1+b1]^c1 + q[a2..a2+b2]^c2 + ..., where (a_i, b_i, c_i) are elements of the list substitutions and q = w+pat,
is a suffix of w[1..j].
If yes, returns the length of the matched Suffix.
*/
int checkSubstitutionSuffix(string pat, string w, int j, vector<tuple<int, int, int>> substitutions, vector<pair<int, int>> varDecomposition) { //j = Prefix w[1..j]; s = Position / Len of s in w+pat
    int m = 0;
    for (tuple<int, int, int> pos : substitutions) m += get<1>(pos)*get<2>(pos); //Total length of substitution
    int n = w.length();
    int ptr = j; //ptr auf w
    int ptrPAT = (*(varDecomposition.end() - 1)).first + (*(varDecomposition.end() - 1)).second;

    for(int i = varDecomposition.size() - 1; i >= 0; i--) { //a xvx = a uvu
        int exponent = varDecomposition[i].second;

        ptr -=  exponent * m; //match substititions to..
        ptrPAT -= exponent; //..x^exponent
        int shift = 0;
        for(tuple<int, int, int> s : substitutions) {
            int substitutionStart = get<0>(s);
            int substitutionLen = get<1>(s);
            int substitutionExponent = get<2>(s);
            if(LCP(w + pat, substitutionStart, ptr+shift) < substitutionLen) return -1; //X^i starts with s
            if(LCP(w + pat, substitutionStart, ptr+shift + substitutionLen) < (substitutionExponent-1) * substitutionLen) return -1;
            shift += substitutionLen*substitutionExponent;
        }
        if(LCP(w + pat, ptr, ptr+m) < (exponent-1) * m) return -1; //Is s-periodic

        i--;
        if(i < 0) break;
        ptr -= varDecomposition[i].second; //Shift both pointers by |w_i|
        ptrPAT -= varDecomposition[i].second;
        if(LCP(w + pat, ptr, n+ptrPAT) < varDecomposition[i].second) return -1;
    }
    //No Match: Return -1, Match: Return len of match
    return j - ptr;
}



/**
Helper method to decide whether a given one-variable block is t-periodic.
Returns integer b, such the period breaks in, or right before w_b.
If b = |varBlockDecomposed| the block is t-periodic.
Ergo find first block that is not of form t^j t'.
*/
int checkVariableBlockPeriodic(vector<pair<int, int>> varBlockDecomposed, pair<int, int> t, pair<int, int> ts, pair<int, int> v, const int n, lcpStructures lcpStruct) {
    const int l = v.second / t.second;
    int b = 1;
    for(auto riter = varBlockDecomposed.rbegin() + 1; riter != varBlockDecomposed.rend(); riter += 2) { //Check right to left where period breaks
        pair<int, int> currBlock = *riter;
        int blockPosInLCP = currBlock.first + n; //Pos of bth block in w+pat
        if(currBlock.second == ts.second) {
            if(LCPP(lcpStruct, ts.first, blockPosInLCP) < ts.second) break;
        }
        else {
            if(LCPP(lcpStruct, blockPosInLCP, t.first) < t.second) break; //Does the block start with t (or t'?!)?
            if(LCPP(lcpStruct, blockPosInLCP, blockPosInLCP + t.second) < currBlock.second-t.second) break; // t periodic?
            int overlen = currBlock.second / t.second;
            if(currBlock.second - overlen * t.second != ts.second) break; //does the block end with t', or rather with |t'|
        }
        b += 2;
    }
    return b;
}

/**
Helper method to align the longest t-periodic suffix of w[1..j] to the t-period breakpoint in the one-variable Block
Returns:
        -1 if matching is not possible;
        length of suffix is matching is possible
*/
int uniquePeriodicMatching(const int b, vector<pair<int, int>> varBlockDecomposed, pair<int, int> t, pair<int, int> ts, int j, pair<int, int> v, pair<int, int> vi, const int n, lcpStructures lcpStruct, string w, string pat) {
    const int l = v.second / t.second;
    int blockLen = varBlockDecomposed.size();
    pair<int, int> breakBlock = varBlockDecomposed[blockLen-b - 1]; //Period breaks either in this block or the one before it

    //We check, where the |t| period breaks inside the block
    // TODO: Prüfe +/- 1 Fehler
    int periodBreakInBlock = min(LCS(w+pat, n + breakBlock.first + breakBlock.second - 1, ts.first + ts.second - 1), ts.second); //Check whether the block ends with t'
    if(periodBreakInBlock >= ts.second) periodBreakInBlock += min(LCS(w+pat, n + breakBlock.first + breakBlock.second - ts.second - 1, t.first + t.second - 1), t.second); //Check whether it continues with t
    if(periodBreakInBlock >= ts.second + t.second) periodBreakInBlock = min(LCS(w+pat, n+breakBlock.first+breakBlock.second-t.second - 1, n+breakBlock.first+breakBlock.second-1) + t.second, breakBlock.second); //If it does, we find where the |t| period breaks

    int constantLen = 0;
    int e = 0; //Number of variables past breakBlock
    for(int p = b-2; p >= 0; p -= 2) {
        constantLen += varBlockDecomposed[blockLen-p - 1].second;
    }
    for(int p = b-1; p >= 0; p -= 2) e += varBlockDecomposed[p].second;

    if(periodBreakInBlock == breakBlock.second) { //Case Double-Dagger
        if(breakBlock.second <= ts.second) {
            //Check whether t' is a suffix of t
            if(LCS(w+pat, t.first + t.second -1,n + breakBlock.first + breakBlock.second -1) >= breakBlock.second) {
                periodBreakInBlock += LCS(w+pat, t.first + t.second - breakBlock.second-1, t.first+t.second-1);
            }
        }
        else {
            //Otherwise we have something of the form t''' t^k t' and we want to check, whether t''' can be extended with a suffix of t, while
            //Preserving the |t|-period
            int plen = (breakBlock.second - ts.second) % t.second;
            periodBreakInBlock += LCS(w+pat, t.first + t.second - plen-1, t.first+t.second-1);
        }
    }
    constantLen += periodBreakInBlock;
    //Longest t periodic suffix of w[1..j]
    int longestTPeriodicSuffix = min(LCS(w+pat, j-t.second-1, t.first+t.second-1), t.second);
    if(longestTPeriodicSuffix == t.second) longestTPeriodicSuffix += LCS(w+pat, j-1, j-t.second-1);
    int k = (longestTPeriodicSuffix - constantLen - (e * vi.second)) / (t.second * e);
    if(k < 0) return -1;
    return checkSubstitutionSuffix(pat, w, j, {{vi.first, vi.second, 1}, {t.first, t.second, k}}, varBlockDecomposed);
}
