#ifndef REGMATCH
#define REGMATCH
#include<vector>
#include<string>
std::vector<std::vector<int>> calcOccurenceMatrix(std::string w, int m, std::vector<std::string> words);
std::pair<std::vector<std::vector<int>>, std::vector<std::vector<int>>> calcDefaultEdges(std::vector<std::vector<int>> occurMatrix, std::vector<std::string> words);
std::vector<std::vector<std::pair<int, int>>> walkNextNode(std::vector<std::vector<int>> occurMatrix, std::vector<std::vector<int>> maxMatchMatrix);
std::pair<std::vector<int>, std::vector<std::string>> factoriseNaive(std::string pat);
std::pair<std::vector<std::vector<int>>, std::vector<std::vector<int>>> returnRegGraph(std::string w, std::string pat);
int numSolutionsReg(std::string w, std::string pat);
bool regDecision(std::string w, std::string pat);
#endif
