#include <string>
#include "../Pattern/noncrossMatching.h"
#include "../Pattern/patternDSA.h"
#include "../Graph/enumgraphNC.h"
#include "../Pattern/regularMatching.h"
#include "../Graph/enumGraphReg.h"
using namespace std;

vector<vector<string>> convertAllToAssignmentsNC(vector<vector<int>> nodes, string w, string pat);
vector<vector<string>> convertAllToAssignmentsReg(vector<vector<int>> nodes, string w, string pat);

/*
This file contains very simple methods, simply wrapping the main methods and calling them with the desired arguments.
*/

/*
Run problem instance and return a string detailing the results.
*/
string runInstance(string w, string pat, bool enumerate, bool nonCross, bool numSols)
{
    string res = "Results for Word w = " + w + " and pattern pat = " + pat + "\n";
    if(nonCross) {
        if(enumerate) {
            res += "Enumerating all assignments explicitly: \n";
            vector<vector<int>> nodePaths = beginEnumNC(w, pat);
            vector<vector<string>> assignments = convertAllToAssignmentsNC(nodePaths, w, pat);
            int num = 1;
            for(vector<string> a : assignments) {
                res += to_string(num) + ":\n";
                for(string s : a) {
                    res += s + " ; ";
                }
                num++;
                res += "\n";
            }
        }
        else {
            bool matchable = nonCrossDecision(w, pat);
            res += "Instance is " + (string) (matchable ?  "matchable" : "non-matchable") + "\n";
        }
        if(numSols) {
            res += "Number of Solutions: " + to_string(nonCrossGraph(w, pat).second) + "\n";
        }
    }
    else {
        if(enumerate) {
            res += "Enumerating all assignments explicitly: \n";
            vector<vector<int>> nodePaths = beginEnumReg(w, pat);
            vector<vector<string>> assignments = convertAllToAssignmentsReg(nodePaths, w, pat);
            int num = 1;
            for(vector<string> a : assignments) {
                res += to_string(num) + ":\n";
                for(string s : a) {
                    res += s + " ; ";
                }
                num++;
                res += "\n";
            }
        }
        else {
            bool matchable = regDecision(w, pat);
            res += "Instance is " + (string) (matchable ?  "matchable" : "non-matchable") + "\n";
        }
        if(numSols) {
            res += "Number of Solutions: " + to_string(numSolutionsReg(w, pat)) + "\n";
        }
    }
    return res;
}


/*
The following methods convert a list of the assigned positions for the words w_i (for regular patterns)
into the concrete substitution.
*/
vector<string> convertToAssignmentReg(vector<int> nodes, vector<string> words, string w) {
    vector<string> res;
    int lastNode = nodes[0];
    for(int i = 1; i < nodes.size(); i++) {
        int newNode = nodes[i];
        int startAssignment = lastNode + words[i-1].length();
        int endAssignment = newNode -1;
        string varAssignment = w.substr(startAssignment, endAssignment-startAssignment + 1);

        res.push_back(varAssignment);
        lastNode = newNode;
    }
    return res;
}

vector<vector<string>> convertAllToAssignmentsReg(vector<vector<int>> nodes, string w, string pat) {
    auto [variables, words] = factoriseNaive(pat);
    vector<vector<string>> res;
    for(vector<int> path : nodes) res.push_back(convertToAssignmentReg(path, words, w));
    return res;
}

/*
The following methods convert a list of the lengths of the substitution (for non-cross patterns)
into the concrete substitution.
*/
vector<string> convertToAssignmentNC(vector<int> nodes, vector<pair<int, int>> variables, vector<pair<int, int>> words, vector<vector<pair<int, int>>> varDecomposed, string w) {
    vector<string> res;
    int lastNode = nodes[0];
    for(int i = 1; i < nodes.size(); i++) {
        int newNode = nodes[i];
        int blockLen = newNode - lastNode - words[i-1].second;
        int numVar = 0;
        for(int k = 0; k  < varDecomposed[i-1].size(); k += 2) numVar += varDecomposed[i-1][k].second;
        int wordLens = 0;
        for(int k = 1; k  < varDecomposed[i-1].size(); k += 2) wordLens += varDecomposed[i-1][k].second;

        int totalVarAssignmentLen = blockLen - wordLens;
        int singleVarAssignmentLen = totalVarAssignmentLen / numVar;

        string varAssignment = w.substr(lastNode+words[i-1].second, singleVarAssignmentLen);
        res.push_back(varAssignment);

        lastNode = newNode;
    }
    return res;
}

vector<vector<string>> convertAllToAssignmentsNC(vector<vector<int>> nodes, string w, string pat) {
    auto [variables, words] = factorisePattern(pat);
    vector<vector<pair<int, int>>> varDecomposed = decomposeVariableBlocks(pat, variables);
    vector<vector<string>> res;
    for(vector<int> path : nodes) res.push_back(convertToAssignmentNC(path, variables, words, varDecomposed, w));
    return res;
}
