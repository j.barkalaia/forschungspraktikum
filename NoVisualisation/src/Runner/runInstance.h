#ifndef runInstance_H
#define runInstance_H
#include <string>
#include <optional>
std::string runInstance(std::string w,std::string pat, bool enumerate, bool nonCross, bool numSols);
#endif
