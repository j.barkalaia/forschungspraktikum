#ifndef commonEnumeration_H
#define commonEnumeration_H
#include <vector>
#include <string>
typedef std::pair<int, int> node;
typedef std::pair<node, node> edge;
typedef std::pair<node, int> defaultWalk;

std::string outputFormat(int m, int l, edge e);
std::vector<int> explicitSol(std::vector<node> path, std::vector<std::vector<int>> occurMatrix);
#endif
