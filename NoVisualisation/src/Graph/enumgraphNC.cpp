#include <iostream>
#include <deque>
#include "../Pattern/noncrossMatching.h"
#include "commonEnumeration.h"
using namespace std;


void enumerate(edge e, int m, vector<pair<edge, int>> *S, int *stackPosition, vector<int> *C, vector<vector<pair<int, int>>> wNextNode, vector<vector<vector< arithmProg >>> edges, vector<vector<int>> nodes, vector<vector<int>> progressionIndices, vector<vector<int>>* explicitPaths);
string outputFormat(int m, int l, edge e);
pair<int, pair<int, int>> getSuccessorNode(node a, pair<int, int> x, vector<vector<vector< arithmProg >>> edges, vector<vector<int>> nodes, vector<vector<int>> progressionIndices);

/*
This file contains the implementation of the enumeration algorithms described in the paper,
for *non-cross* patterns. It is very similar to the implementation for non-cross patterns, however
for clarity the algorithms are implemented seperately and a little differently.
*/



/*
Initiates the enumeration of the graph given by the instance non-cross (w, pat).
To that end the preprocessing is done using walkNextNode.
Afterwards enumerate is called on every outgoing edge of the origin node (i.e. the node at position {0, 0})

Returns all paths in form of paths of the labels of the nodes.
*/
vector<vector<int>> beginEnumNC(string w, string pat) {
    auto [nodes, edges, progressionIndices] = nonCrossGraph(w, pat).first;
    vector<vector<pair<int, int>>> wnn = walkNextNode(nodes, edges, progressionIndices);
    int m = edges.size();
    pair<int, int> explicitNextEdge = {0, 0};
    vector<vector<int>> explicitPaths;
    pair<int, pair<int, int>> nextIt =  getSuccessorNode(node{0, 0}, explicitNextEdge, edges, nodes, progressionIndices);
    int newBranchNode = nextIt.first;
    explicitNextEdge = nextIt.second;
    while(newBranchNode != -1) {
        vector<pair<edge, int>> S(m);
        vector<int> C;
        int stackPosition = 0;
        edge e = make_pair(make_pair(0, 0), make_pair(1, newBranchNode));
        enumerate(e, m, &S, &stackPosition, &C, wnn, edges, nodes, progressionIndices, &explicitPaths);

        nextIt =  getSuccessorNode(node{0, 0}, explicitNextEdge, edges, nodes, progressionIndices);
        newBranchNode = nextIt.first;
        explicitNextEdge = nextIt.second;
    }
    return explicitPaths;
}


/*
Return explicit path of nodes on the default graph, starting from node v with length l
*/
vector<node> explicitDefaultWalk(node v, int l, vector<vector<vector< arithmProg >>> edges) {
    vector<node> res;
    res.push_back(v);
    for(int i = 0; i < l; i++) {
        v = node{v.first+1, get<0>(edges[v.first][v.second][0])};
        res.push_back(v);
    }
    return res;
}

/*
Return explicit representation represented by the prefix of length l of the path represented by S
*/
vector<node> explicitPath(int m, int l, edge e, vector<pair<edge, int>> S, int stackPos, vector<vector<vector< arithmProg >>> edges) {
    vector<node> res;
    int pos = 0; //Tracks how many nodes have already been traversed
    int prefix_len = m-l; //Traverse this many nodes on the path described by S
    pair<edge, int> currentWalk = S[0];
    res.push_back((currentWalk.first).first); pos++;
    for(int i = 1; i <= stackPos && pos < prefix_len; i++) { //Since we do not modify the length of the default walks on S we need to care to follow the default path to the next branch
        node currentNode = currentWalk.first.second;
        pair<edge, int> nextWalk = S[i];
        node nextNode = nextWalk.first.first;
        vector<node> nextPath = explicitDefaultWalk((currentWalk.first).second, min(prefix_len-pos-1, nextNode.first - currentNode.first), edges);
        pos += min(prefix_len-pos, nextNode.first - currentNode.first + 1);
        res.insert(res.end(), nextPath.begin(), nextPath.end());
        currentWalk = nextWalk;
    }
    if(prefix_len-pos==0) return res;
    vector<node> nextPath = explicitDefaultWalk(currentWalk.first.second, prefix_len-pos, edges);
    res.insert(res.end(), nextPath.begin(), nextPath.end());
    return res;
}



/*
Given an implicit definition of a path, returns the explicit path as as a path of the labels of the nodes.
*/
vector<int> retExplicitSol(int m, int l, edge e, vector<pair<edge, int>> S, int stackPosition, vector<vector<vector< arithmProg >>> edges, vector<vector<int>> occurMatrix) {
    vector<node> path = explicitPath(m, l, e, S, stackPosition, edges); //First part of path - Until branching
    vector<node> newPath = explicitDefaultWalk(e.second, l, edges); //Second part of path - From branch until end
    path.insert(path.end(), newPath.begin(), newPath.end());
    return explicitSol(path, occurMatrix);
}


/*
Return the x-th successor node of node a and the index of the next possible edge of a.
The index x is represented as a pair(y, z) which represents that the z-th element of the y-th (possibly periodic) edge is to be returned.
*/
pair<int, pair<int, int>> getSuccessorNode(node a, pair<int, int> x, vector<vector<vector< arithmProg >>> edges, vector<vector<int>> nodes, vector<vector<int>> progressionIndices) {
    if(edges[a.first][a.second].size() <= x.first) return make_pair(-1, make_pair(-1, -1));
    arithmProg currEdge = edges[a.first][a.second][x.first];
    //In this case the edge is non-periodic and the case is trivial
    if(get<1>(currEdge) == -1) {
        if(x.second > 0) return getSuccessorNode(a, make_pair(x.first + 1, 0), edges, nodes, progressionIndices);
        else return make_pair(get<0>(currEdge), make_pair(x.first + 1, 0));
    }
    //In this case the edge has the period 1.
    //Simply check whether we have another successor node.
    else if(get<1>(currEdge) == 1) {
        if(get<0>(currEdge) + x.second < nodes[a.first+1].size()) {
            return make_pair(get<0>(currEdge) + x.second, make_pair(x.first, x.second +1));
        }
        else {
            return getSuccessorNode(a, make_pair(x.first + 1, 0), edges, nodes, progressionIndices);
        }
    }
    //In this case the edge has a non-trivial period (i.e. not period 1).
    //We computed beforehand, all members of the specific arithmetic progression
    //Hence we can simply check whether there are anymore elements availible in that progression
    else {
        auto [progIndex, progPosIndex] = get<3>(currEdge).value();
        if(progPosIndex + x.second < progressionIndices[progIndex].size()) {
            return make_pair(progressionIndices[progIndex][progPosIndex+x.second], make_pair(x.first, x.second+1));
        }
        else {
            return getSuccessorNode(a, make_pair(x.first + 1, 0), edges, nodes, progressionIndices);
        }
    }
}

/*
Main enumeration method. Check thesis text for detailed description.
*/
void enumerate(edge e, int m, vector<pair<edge, int>> *S, int *stackPosition, vector<int> *C,vector<vector<pair<int, int>>> wNextNode, vector<vector<vector< arithmProg >>> edges, vector<vector<int>> nodes, vector<vector<int>> progressionIndices, vector<vector<int>>* explicitPaths) {
    top: //Marker for GOTO allowing for guaranteed Tail-Call Optimization
        int l = m - (e.second).first;
        (*S)[*stackPosition] = make_pair(e, l);
        (*stackPosition)++;
        (*C).push_back(*stackPosition);
        cout << outputFormat(m, l, e) << endl;
        (*explicitPaths).push_back(retExplicitSol(m, l, e, *S, *stackPosition, edges, nodes));

        //If nothing is left to enumerate, we can pop the currently added walk
        if(l == 0) {
            (*C).pop_back();
            return;
        }

        deque<node> U;
        node v = e.first;
        node u = e.second;
        node next = wNextNode[u.first][u.second];
        edge last;
        if(next.first != -1) { //If there is another branching node we will definitely have a tail-call -> save it now for later
            U.push_back(u);
            //Find first branching edge of next node
            auto [lastNodeIndex, lastNodePos] = getSuccessorNode(next, {0, 1}, edges, nodes, progressionIndices);
            node lastNode = make_pair(next.first+1, lastNodeIndex);
            last = make_pair(next, lastNode); //Executed for the tail call
        }
        else { //Otherwise we are done with the current stack-frame
            (*C).pop_back();
            (*stackPosition)--;
            return;
        }
        while(!U.empty()) {
            node s = U.front();
            U.pop_front();
            node vs = wNextNode[s.first][s.second];
            //We possibly have (atleast one) more branching nodes after vs
            //If yes, we will also need to enumerate that node, hence we save it in U
            node vss = make_pair(vs.first+1, getSuccessorNode(vs, {0, 0}, edges, nodes, progressionIndices).first);;
            if(wNextNode[vss.first][vss.second].first != -1) U.push_back(vss);


            //Enumerate the branching edges of vs
            pair<int, int> currExplicitEdge = {0, 1};
            //If we have already saved the first branching edge of vs as last, we skip this edge
            if(s == u) currExplicitEdge = getSuccessorNode(vs, currExplicitEdge, edges, nodes, progressionIndices).second;
            pair<int, pair<int, int>> nextIt =  getSuccessorNode(vs, currExplicitEdge, edges, nodes, progressionIndices);
            int newBranchNode = nextIt.first;
            currExplicitEdge = nextIt.second;
            while(newBranchNode != -1) {
                edge b = edge{vs, node{vs.first+1, newBranchNode} };
                enumerate(b, m, S, stackPosition, C, wNextNode, edges, nodes, progressionIndices, explicitPaths);

                nextIt =  getSuccessorNode(vs, currExplicitEdge, edges, nodes, progressionIndices);
                newBranchNode = nextIt.first;
                currExplicitEdge = nextIt.second;
                *stackPosition = (*C).back();
            }
        }
        (*C).pop_back();
    e = last;
    goto top; //Execute Tail-Call
    return;
}
