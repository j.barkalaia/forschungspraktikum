#include<string>
#include<fstream>
#include<iostream>
#include<optional>
#include<unordered_map>
#include<functional>
#include "Runner/runInstance.h"
using namespace std;

/*
Main executable, mainly deals with parsing the arguments and reading/writing to the files.
*/

struct Settings {
  bool enumerate {false};
  bool visualize {false};
  bool nonCross {true};
  bool numSols {false};
  string infile {"testCases.txt"}; //default file for test-cases
  optional<string> outfile;
};

typedef function<void(Settings&)> NoArgumentHandle;

#define S(str, f, v) {str, [](Settings& s) {s.f = v;}}
const unordered_map<string, NoArgumentHandle> NoArgument {
  S("--visualize", visualize, true),
  S("-v", visualize, true),

  S("--regular", nonCross, false),
  S("-r", nonCross, false),

  S("--enumerate", enumerate, true),
  S("-e", enumerate, true),

  S("--numsols", numSols, true),
  S("-n", numSols, true),
};
#undef S

typedef function<void(Settings&, const string&)> OneArgumentHandle;

#define S(str, f, v) \
  {str, [](Settings& s, const string& arg) { s.f = v; }}

const unordered_map<string, OneArgumentHandle> OneArgument {
  S("-o", outfile, arg),
  S("--output", outfile, arg),

  S("-i", infile, arg),
  S("--infile", infile, arg),
};
#undef S


Settings parseSettings(int argc, const char* argv[]) {
  Settings settings;

  for(int i {1}; i < argc; i++) {
    string opt {argv[i]};

    // Check whether option has an argument, and handle
    if(auto j {NoArgument.find(opt)}; j != NoArgument.end())
      j->second(settings); // Yes, handle it!

    else if(auto k {OneArgument.find(opt)}; k != OneArgument.end())
      if(++i < argc)
        k->second(settings, {argv[i]});
      else
        throw std::runtime_error {"no param after flag " + opt};

    else
      cerr << "following flag is unknown: " << opt << endl;
  }

  return settings;
}


int main(int argc, const char* argv[]) {
    Settings settings = parseSettings(argc, argv);
    if(!settings.outfile.has_value()) cout << "Please provide output filename with the -o flag";
    string line;
      ifstream infile (settings.infile);
      ofstream outfile (settings.outfile.value());
      if (infile.is_open())
      {
        while ( getline (infile,line) )
        {
          int delimiter = line.find(",");
          string w = line.substr(0, delimiter);
          string pat = line.substr(delimiter+1, line.length()-delimiter-1);

          string res = runInstance(w, pat, settings.visualize, settings.enumerate, settings.nonCross, settings.numSols);
        outfile << res;
        outfile << "-------------------------------------------------------------------------------------";
        outfile << "\n";
        }
        infile.close();
        outfile.close();
      }

      else cerr << "Unable to open file" << endl;
}
