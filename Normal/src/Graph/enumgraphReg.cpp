#include <bits/stdc++.h>
#include "../Pattern/regularMatching.h"
#include "commonEnumeration.h"
using namespace std;

void enumerate(edge e, int m, vector<pair<edge, int>> *S, int *stackPosition, vector<int> *C, vector<vector<pair<int, int>>> wNextNode, vector<vector<int>> maxMatchMatrix, vector<vector<int>> occurMatrix, vector<vector<int>>* explicitPaths);
string outputFormat(int m, int l, edge e);

/*
This file contains the implementation of the enumeration algorithms described in the paper,
for *regular* patterns. It is very similar to the implementation for non-cross patterns, however
for clarity the algorithms are implemented seperately and a little differently.
*/


/*
Initiates the enumeration of the graph given by the instance non-cross (w, pat).
To that end the preprocessing is done using walkNextNode.
Afterwards enumerate is called on every outgoing edge of the origin node (i.e. the node at position {0, 0})

Returns all paths in form of paths of the labels of the nodes.
*/
vector<vector<int>> beginEnumReg(string w, string pat) {
    auto [variables, words] = factoriseNaive(pat);
    auto [nodes, edges] = returnRegGraph(w, pat);
    auto wnn = walkNextNode(nodes, edges);
    int m = edges.size();
    vector<vector<int>> explicitPaths;
    for(int i = edges[0][0]; i < nodes[1].size(); i++) {
        vector<pair<edge, int>> S(m);
        vector<int> C;
        int stackPosition = 0;
        edge e = {{0, 0}, {1, edges[0][0] + i}};
        enumerate(e, m, &S, &stackPosition, &C, wnn, edges, nodes, &explicitPaths);
    }
    return explicitPaths;
}



/*
Return explicit path of nodes on the default graph, starting from node v with length l
*/
vector<node> explicitDefaultWalk(node v, int l, vector<vector<int>> maxMatchMatrix) {
    vector<node> res;
    res.push_back(v);
    for(int i = 0; i < l; i++) {
        v = node{v.first+1, maxMatchMatrix[v.first][v.second]};
        res.push_back(v);
    }
    return res;
}

/*
Return explicit representation represented by the prefix of length l of the path represented by S
*/
vector<node> explicitPath(int m, int l, edge e, vector<pair<edge, int>> S, int stackPos, vector<vector<int>> maxMatchMatrix) {
    vector<node> res;
    int pos = 0; //Tracks how many nodes have already been traversed
    int prefix_len = m-l; //Traverse this many nodes on the path described by S
    pair<edge, int> currentWalk = S[0];
    res.push_back((currentWalk.first).first); pos++;
    for(int i = 1; i <= stackPos && pos < prefix_len; i++) { //Since we do not modify the length of the default walks on S we need to care to follow the default path to the next branch
        node currentNode = currentWalk.first.second;
        pair<edge, int> nextWalk = S[i];
        node nextNode = nextWalk.first.first;
        vector<node> nextPath = explicitDefaultWalk((currentWalk.first).second, min(prefix_len-pos-1, nextNode.first - currentNode.first), maxMatchMatrix);
        pos += min(prefix_len-pos, nextNode.first - currentNode.first + 1);
        res.insert(res.end(), nextPath.begin(), nextPath.end());
        currentWalk = nextWalk;
    }
    if(prefix_len-pos==0) return res;
    vector<node> nextPath = explicitDefaultWalk(currentWalk.first.second, prefix_len-pos, maxMatchMatrix);
    res.insert(res.end(), nextPath.begin(), nextPath.end());
    return res;
}




/*
Given a path on the graph, converts it into a representation of assignments from the w_i to positions in w
*/
vector<int> retExplicitSol(int m, int l, edge e, vector<pair<edge, int>> S, int stackPosition, vector<vector<int>> maxMatchMatrix, vector<vector<int>> occurMatrix) {
    vector<node> path = explicitPath(m, l, e, S, stackPosition, maxMatchMatrix); //First part of path - Until branching
    vector<node> newPath = explicitDefaultWalk(e.second, l, maxMatchMatrix); //Second part of path - From branch until end
    path.insert(path.end(), newPath.begin(), newPath.end());
    return explicitSol(path, occurMatrix);
}


/*
Main enumeration method. Check thesis text for detailed description.
*/
void enumerate(edge e, int m, vector<pair<edge, int>> *S, int *stackPosition, vector<int> *C,vector<vector<pair<int, int>>> wNextNode, vector<vector<int>> maxMatchMatrix, vector<vector<int>> occurMatrix, vector<vector<int>>* explicitPaths) {
    top: //Marker for GOTO allowing for guaranteed Tail-Call Optimization
        int l = m - (e.second).first;
        (*S)[*stackPosition] = make_pair(e, l);
        (*stackPosition)++;
        (*C).push_back(*stackPosition);
        cout << outputFormat(m, l, e) << endl;
        (*explicitPaths).push_back(retExplicitSol(m, l, e, *S, *stackPosition, maxMatchMatrix, occurMatrix));

         //If nothing is left to enumerate, we can pop the currently added walk
        if(l == 0) {
            (*C).pop_back();
            return;
        }
        deque<node> U;

        node v = e.first;
        node u = e.second;
        node next = wNextNode[u.first][u.second];
        edge last;
        if(next.first != -1) { //If there is another branching node we will definitely have a tail-call -> save it now for later
            U.push_back(u);
            node lastNode = make_pair(next.first+1, maxMatchMatrix[next.first][next.second] + 1);
            last = make_pair(next, lastNode); //Executed for the tail call
        }
        else { //Otherwise we are done with the current stack-frame
            (*C).pop_back();
            (*stackPosition)--;
            return;
        }
        while(!U.empty()) {
            node s = U.front();
            U.pop_front();
            node vs = wNextNode[s.first][s.second];

            //We possibly have (atleast one) more branching nodes after vs
            //If yes, we will also need to enumerate that node, hence we save it in U
            node vss = make_pair(vs.first+1, maxMatchMatrix[vs.first][vs.second]);
            if(wNextNode[vss.first][vss.second].first != -1) U.push_back(vss); //If exists, add walks after branching node vs that contain branches

            //Enumerate the branching edges of vs

            int x;
            if(s != u) x = 1;
            else x = 2; //In case we are looking at the default walk, we have already considered the second walk in our tail call

            while(maxMatchMatrix[vs.first][vs.second]+x < occurMatrix[vs.first+1].size()) {
                edge b = edge{vs, node{vs.first+1, maxMatchMatrix[vs.first][vs.second]+x}};
                enumerate(b, m, S, stackPosition, C, wNextNode, maxMatchMatrix, occurMatrix, explicitPaths);
                x++;
                *stackPosition = (*C).back();
            }
        }
        (*C).pop_back();

    e = last;
    goto top; //Execute Tail-Call
    return;
}
