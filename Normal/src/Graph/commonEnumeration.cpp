#include <string>
#include <vector>
using namespace std;
typedef pair<int, int> node;
typedef pair<node, node> edge;
typedef pair<node, int> defaultWalk;

/**
Common Methods and definitions for both regular and NC enumeration
*/


/*
Helper function.
Prints current path in format:
Length to traverse on Stack S,
branching edge (u/v),
distance to traverse on default graph from node v
*/
string outputFormat(int m, int l, edge e) {
    string res = to_string((m-l-1));
    node n1 = e.first; node n2 = e.second;
    res += ", (" +  to_string(n1.first) + "/" + to_string(n1.second) + " | ";
    res +=  to_string(n2.first) + "/" + to_string(n2.second) + "), " + to_string(l);
    return res;
}

/*
Convert path on the graph into the path of labels
*/
vector<int> explicitSol(vector<node> path, vector<vector<int>> occurMatrix) {
    vector<int> res;
    for(node a : path) res.push_back(occurMatrix[a.first][a.second]);
    return res;
}
