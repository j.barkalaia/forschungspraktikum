#include<vector>
#include<string>
#include<algorithm>
#include <boost/container_hash/hash.hpp>
#include "Primitivity.h"
#include "stringDSA.h"
#include "../String/SuffixArray/Skew.h"
#include<vector>
using namespace std;

/**
This file contains the algorithms for calculating the sets R, see thesis for details.
*/

int uniquePeriod(pair<int, int> t, int j, int i, int pow, tuple<int, int, int> occ_prefix, vector<vector<int>> DBF, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, lcpStructures lcpStruct) {

    pair<int, int> x = {get<0>(occ_prefix), get<1>(occ_prefix)};
    int alpha = (get<2>(occ_prefix) + pow - x.first) / x.second;

    //Find longest |x|-periodic prefix of w[j+1..n]
    int longestxPeriod = LCPP(lcpStruct, x.first, j+1);
    if(longestxPeriod >= x.second) longestxPeriod = x.second + LCPP(lcpStruct, j+1, j+1+x.second);
    int beta = longestxPeriod / x.second;

    //Only possibility for a k-extension: x^\beta x' y'
    int start = get<0>(occ_prefix) + (alpha-beta)*x.second;
    if(LCPP(lcpStruct, start, j+1) >= i - start) {
        if((isPrim({start, i-1}, H, DBF))) {
            return i-start;
        }
    }
    return -1;
}

vector<int> constantSizeOccSet(int j, int i, tuple<int, int, int> occ_prefix, vector<vector<int>> DBF, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, lcpStructures lcpStruct) {
    vector<int> res;
    if(get<1>(occ_prefix) <= -1 || get <2>(occ_prefix) <= -1 || get<0>(occ_prefix) == get<2>(occ_prefix) || get<0>(occ_prefix) + get<1>(occ_prefix) == get<2>(occ_prefix)) { //Fall |occ| <= 2
        //Definitely one t occurence
        if(LCPP(lcpStruct, get<0>(occ_prefix), j+1) >= i - get<0>(occ_prefix)) {
            if(isPrim({get<0>(occ_prefix), j}, H, DBF)) res.push_back(i - get<0>(occ_prefix));
        }
        //Incase we have another
        if(get<2>(occ_prefix) != -1 && get<0>(occ_prefix) != get<2>(occ_prefix)) {
            if(LCPP(lcpStruct, get<2>(occ_prefix), j+1) >= i - get<2>(occ_prefix)) {
                if(isPrim({get<2>(occ_prefix), j}, H, DBF)) res.push_back(i - get<2>(occ_prefix));
            }
        }
    }
    return res;
}

vector<vector<int>> primitiveExtensions(string w, string v, lcpStructures lcpStruct) {
    const unsigned int n = w.length();
    vector<vector<int>> DBF = calcDBF(w);
    unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H = constructH(DBF);
    vector<vector<int>> R(n+1);
    if(findPeriod(v) * 6 < v.length()) {
        int i = w.find(v, 0);
        while(i != string::npos) {
            int pow = 1;
            int j = i + v.length()-1;
            pair<int, int> v_1 = {j - findPeriod(v) + 1, findPeriod(v)};
            int s = v.length() / v_1.second;
            pair<int, int> v_2 = {i, v.length() - s*v_1.second};

            //Case A
            if(findPeriod(v) * 6 < v.length()) {
                for(int k = 0; k < __bit_width(n) && pow <= i && pow <= n-j-1; k++) {
                    pair<int, int> t = {j+1, j + pow};
                    tuple<int, int, int> occ_prefix = Occ(t, {i - 2*pow, i-1}, H, DBF);
                    if(get<0>(occ_prefix) <= -1) {pow *= 2; continue;} //No occurence of t
                    get<0>(occ_prefix) += max(i-2*pow, 0);
                    if(get<2>(occ_prefix) != -1) get<2>(occ_prefix) += max(i-2*pow, 0);
                    //Case y' != eps
                    if(get<1>(occ_prefix) <= -1 || get <2>(occ_prefix) <= -1 || get<0>(occ_prefix) == get<2>(occ_prefix) || get<0>(occ_prefix) + get<1>(occ_prefix) == get<2>(occ_prefix)) { //Fall |occ| <= 2
                        vector<int> res = constantSizeOccSet(j, i, occ_prefix, DBF, H, lcpStruct);
                        for(int r : res) {
                            int prefix_len = j+ r + 1;
                            if(R[prefix_len].empty() || R[prefix_len].back() != r) R[prefix_len].push_back(r);
                        }
                        pow *= 2;
                        continue;
                    }
                    pair<int, int> x = {get<0>(occ_prefix), get<1>(occ_prefix)};
                    int alpha = (get<2>(occ_prefix) + pow - x.first) / x.second;
                    if(min(LCP(w, get<2>(occ_prefix) + (pow / x.second) * x.second, x.first), (i - (get<0>(occ_prefix)+alpha*get<1>(occ_prefix)))) >= x.second) { // Und LCP kleiner als Rest <- FIXED
                        alpha++;
                    }
                    pair<int, int> x_alpha = {get<0>(occ_prefix), alpha*x.second};
                    int rest = i - (x_alpha.first + x_alpha.second);
                    pair<int, int> x_s = {x_alpha.first + x_alpha.second, min(LCP(w, get<0>(occ_prefix), x_alpha.first + x_alpha.second), rest)};

                    //Case A.1
                    if(x_s.second != rest) {
                        int res = uniquePeriod(t, j, i, pow, occ_prefix, DBF, H, lcpStruct);
                        if(res != -1) {
                            int prefix_len = j + res + 1;
                            if(R[prefix_len].empty() || R[prefix_len].back() != res) R[prefix_len].push_back(res);
                        }
                    }
                    //Case A.2
                    else {
                        bool alwaysPeriod = false;
                        int maxSuffixPower = (n - j - 1 -  x_s.second) / x.second; //- x_s.second um
                        int minPower = ((pow-x_s.second) / x.second) + ((pow-x_s.second) % x.second != 0);
                        for(int g = minPower; g <= min(min(5, alpha), maxSuffixPower); g++) { //Check all |t|/|x| <= gamma <= 5 for x^gamma x' v_2(v_1)^sigma
                            int start = get<0>(occ_prefix) + (alpha-g)*x.second;
                            if(isPrim({start, j}, H, DBF)) {
                                int prefix_len = j + (i - start) + 1;
                                if(prefix_len > n) break;
                                if(LCP(w, start, j+1) >= i-start) {
                                    if(R[prefix_len].empty() || R[prefix_len].back() != i-start) R[prefix_len].push_back(i-start);
                                }
                            }
                        }
                        if(min(alpha, maxSuffixPower) <= 5) {
                            pow *= 2;
                            continue;
                        }
                        //gamma >= max(|t|/|x|, 6)

                        //Case x = v_1 = z:
                        if(v_1.second == x.second && LCP(w, x.first, v_1.first) >= v_1.second) { //x == v_1
                            if(x_s.second == 0 && v_2.second == 0) alwaysPeriod = true; //Trivialer Fall wenn x' v_2 = ""
                            else if((x_s.second + v_2.second == x.second) && LCP(w, x_s.first, x.first) >= x.second) alwaysPeriod = true;
                        }
                        //Case x = z, r = 1
                        pair<int, int> zss = {x.first + x_s.second, (x.second - x_s.second) % x.second}; //Modulo for case x_s.second == 0
                        if(v.length() == zss.second + x.second) {
                            if(LCP(w, zss.first, i) >=  zss.second) {
                                if(LCP(w, i+zss.second, x.first) >= x.second) alwaysPeriod = true;
                            }
                        }

                        vector<int> notPrimPower;
                        //Case v_1 = z, r = 1
                        //Max. 1 Val for gamma s.t. x^g x' = v_1 z'
                        pair<int, int> zs = {v_1.first, v_1.second - v_2.second};
                        int g = (zs.second + v_1.second - x_s.second) / x.second;
                        if((g >= minPower) && (g*x.second + x_s.second == v_1.second + zs.second)) {
                            if(LCP(w, get<0>(occ_prefix) + (alpha-g)*x.second, v_1.first) >= v_1.second) {
                                if(LCP(w, get<0>(occ_prefix) + (alpha-g)*x.second + v_1.second, zs.first) >= zs.second) notPrimPower.push_back(g);
                            }
                        }

                        //Case x != v_1 != z
                        //Case s = 2:
                        //Case |x^g x'| >= |v_2 v_1^s|
                        for(int gs = s-1; gs <= s+2; gs++) {
                            for(int l : {0, 1}) {
                                int g = gs + l;
                                int start = get<0>(occ_prefix) + (alpha-g)*x.second;
                                int len = j - start + 1;
                                if((len % 2 == 0) && LCP(w, start, start + len/2) >= len/2) notPrimPower.push_back(g); //Check square
                            }
                        }

                        int ptr = 0;
                        if(!alwaysPeriod) {
                            for(int f = max(6, ((pow-x_s.second) / x.second) + ((pow-x_s.second) % x.second != 0)); f <= min(alpha, maxSuffixPower); f++) {
                                int start = get<0>(occ_prefix) + (alpha-f)*x.second;
                                if(ptr < notPrimPower.size() && notPrimPower[ptr] == f) ptr++;
                                else {
                                    int prefix_len = j + (i - start) + 1;
                                    if(LCP(w, start, j+1) >= i-start) {
                                        if(R[prefix_len].empty() || R[prefix_len].back() != i-start) R[prefix_len].push_back(i-start);
                                    }
                                }
                            }
                        }
                    }
                    pow *= 2;
                }
            }


            //Case B
            else {
                unsigned int maxExtension  = min((unsigned int) i, n-j);
                for(int l = 1; l <= min((unsigned int) (4*v.length()), maxExtension); l++) {
                    if(LCP(w, i-l, j+1) >= l) {
                        if(isPrim({i-l, j}, H, DBF)) {
                            int prefix_len = j + l + 1;
                            if(R[prefix_len].empty() || R[prefix_len].back() != l) R[prefix_len].push_back(l);
                        }
                    }
                }

                if(maxExtension > 4*v.length()) {
                    for(int k = __bit_width(4*v.length()); k < __bit_width(n) && pow <= i && pow <= n-j-1; k++) {
                        pair<int, int> t = {j+1, j + pow};
                        tuple<int, int, int> occ_prefix = Occ(t, {i - 2*pow, i-1}, H, DBF);
                        if(get<0>(occ_prefix) <= -1) {pow *= 2; continue;} //No occurence of t -> no k-extension
                        get<0>(occ_prefix) += max(i-2*pow, 0);
                        if(get<2>(occ_prefix) != -1) get<2>(occ_prefix) += max(i-2*pow, 0);
                        //Case y' != eps
                        if(get<1>(occ_prefix) <= -1 || get <2>(occ_prefix) <= -1 || get<0>(occ_prefix) == get<2>(occ_prefix) || get<0>(occ_prefix) + get<1>(occ_prefix) == get<2>(occ_prefix)) { //Case |occ| <= 2
                            vector<int> res = constantSizeOccSet(j, i, occ_prefix, DBF, H, lcpStruct);
                            for(int r : res) {
                                int prefix_len = j+ r + 1;
                                if(R[prefix_len].empty() || R[prefix_len].back() != r) R[prefix_len].push_back(r);
                            }
                            pow *= 2;
                            continue;
                        }

                        pair<int, int> x = {get<0>(occ_prefix), get<1>(occ_prefix)};
                        int alpha = (get<2>(occ_prefix) + pow - x.first) / x.second;
                        if(min(LCP(w, get<2>(occ_prefix) + (pow / x.second) * x.second, x.first), (i - (get<0>(occ_prefix)+alpha*get<1>(occ_prefix)))) >= x.second) { // Und LCP kleiner als Rest <- FIXED
                            alpha++;
                        }
                        pair<int, int> x_alpha = {get<0>(occ_prefix), alpha*x.second};
                        int rest = i - (x_alpha.first + x_alpha.second);
                        pair<int, int> x_s = {x_alpha.first + x_alpha.second, min(LCP(w, get<0>(occ_prefix), x_alpha.first + x_alpha.second), rest)};

                        //Case B.1
                        if(x_s.second != rest) {
                            int res = uniquePeriod(t, j, i, pow, occ_prefix, DBF, H, lcpStruct);
                            if(res != -1) {
                                int prefix_len = j + res + 1;
                                if(R[prefix_len].empty() || R[prefix_len].back() != res) R[prefix_len].push_back(res);
                            }
                        }

                        else {
                            bool alwaysPeriod = false;
                            vector<int> notPrimPower;
                            int maxSuffixPower = (n - j - 1 -  x_s.second) / x.second; //- x_s.second um

                            //Check whether v = x'' x^r with $x = x' x''$
                            pair<int, int> xss = {x.first + x_s.second, (x.second - x_s.second) % x.second}; //Modulo for case x_s.second == 0
                            if(LCPP(lcpStruct, xss.first, i) >= xss.second &&
                                LCPP(lcpStruct, i + xss.second, x.first) >= x.second &&
                                LCPP(lcpStruct, i+xss.second, i+xss.second + x.second) >= v.length() - x.second &&
                                (v.length() - xss.second) % x.second == 0) {
                                    alwaysPeriod = true;
                            }

                            for(int g = (v.length() / x.second) - 1; g <= (v.length() / x.second)+2; g++) {
                                int start = get<0>(occ_prefix) + (alpha-g)*x.second;
                                int len = j - start + 1;
                                if((len % 2 == 0) && LCP(w, start, start + len/2) >= len/2) notPrimPower.push_back(g); //Check square
                            }

                            int ptr = 0;
                            if(!alwaysPeriod) {
                                for(int f = max(6, ((pow-x_s.second) / x.second) + ((pow-x_s.second) % x.second != 0)); f <= min(alpha, maxSuffixPower); f++) {
                                    int start = get<0>(occ_prefix) + (alpha-f)*x.second;
                                    if(ptr < notPrimPower.size() && notPrimPower[ptr] == f) ptr++;
                                    else {
                                        int prefix_len = j + (i - start) + 1;
                                        if(LCP(w, start, j+1) >= i-start) {
                                            if(R[prefix_len].empty() || R[prefix_len].back() != i-start) R[prefix_len].push_back(i-start);
                                        }
                                    }
                                }
                            }
                        }
                        pow *= 2;
                    }
                }
            }
            i = w.find(v, i+1);
        }
    }
    return R;
}






vector<vector<vector<int>>> calcR_efficient(vector<pair<int, int>> variables, vector<vector<pair<int, int>>> varDecomposed, string w, string pat) {
    const int s = variables.size();
    const int n = w.length();

    Skew SAw;
    SAw.addString(w);
    SAw.makeSuffixArray();
    struct lcpStructures lcpStruct;
    lcpStruct.LCP =(SAw.makeLCPArray()).first;
    lcpStruct.invertedSA = (SAw.makeLCPArray()).second;
    lcpStruct.st = calcSparseTableMin(lcpStruct.LCP);

    vector<vector<vector<int>>> R(s, vector<vector<int>>(n+1));
    //Berechne R für alle Wörter, für alle 1 <= j <= n
    int i = 0;
    for(vector<pair<int, int>> var : varDecomposed) {
        if(var.size() > 1) {
            pair<int, int> w_pi = *(var.end() - 2);
            R[i] = primitiveExtensions(w, pat.substr(w_pi.first, w_pi.second), lcpStruct);
        }
        i++;
    }
    return R;
}
