#include<vector>
#include<unordered_map>
#include<boost/container_hash/hash.hpp>
#include<iostream>
using namespace std;

/*
Find largest power 2^k such that 2^k <= x
*/
uint32_t highestPowerOfTwoIn(uint32_t x)
{
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x ^ (x >> 1);
}



/*
Given Arithmetic Progression Prog = (Start, Period, End) and Index i, find minimum Index j such that
(1) j is in Prog
(2) j >= i
if there isnt any such j return -1

Time: O(1)
*/
int succInProgression(tuple<int, int, int> prog, int i) {
    int period = get<1>(prog);
    int start = get<0>(prog);
    int end = get<2>(prog);
    if(i <= start) return start;
    int offset = (((start - i) % period) + period) % period;
    if(i + offset <= end) return i+offset;
    return -1;
}

/*
Given Arithmetic Progression Prog = (Start, Period, End) and Index i, find maximum Index j such that
(1) j is in Prog
(2) j <= i
if there isnt any such j return -1

Time: O(1)
*/
int predInProgression(tuple<int, int, int> prog, int i) {
    int period = get<1>(prog);
    int start = get<0>(prog);
    int end = get<2>(prog);
    if(i >= end) return end;
    int offset = (((i-end) % period) + period) % period;
    if(i - offset >= start) return i-offset;
    return -1;
}

bool elementOfProgression(tuple<int, int, int> prog, int i) {
    int period = get<1>(prog);
    int start = get<0>(prog);
    int end = get<2>(prog);
    if(period == -1) return(i == end || i == start);
    else return (succInProgression(prog, i) == i);
}

tuple<int, int, int> twoElementsInProgression(tuple<int, int, int> prog, int x, int y, int d) {
    int element_one = ((!(elementOfProgression(prog, x)) || x == d) ? -1 : x);
    int element_two = -1;
    if(y != -1) element_two = ((!(elementOfProgression(prog, y)) || y == d) ? -1 : y);
    if(element_one == -1) return tuple<int, int, int>{element_two, -1, element_two};
    return tuple<int, int, int>{element_one, -1, max(element_one, element_two)};
}

/*
See Kociumaka 2012, p. 288 for source

Returns the minimal Index j in [i, i + |v|] such that w[j, j + |v| - 1] = v (find first occurence of v in the interval)
if such a j doesnt exist return -1

Time: Avg O(1)
*/
int SUCC(int i, pair<int, int> v, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) {
    int v_len = v.second - v.first + 1;
    unsigned int log_len = __bit_width((unsigned int) v_len) - 1;
    if(H.contains(tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len))) {
        tuple<int, int, int> first_prog = H[tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len)];
        int firstOccur = succInProgression(first_prog, i);
        if(firstOccur != -1 && firstOccur <= i + v_len) {
            return firstOccur;
        }
    }
    if(H.contains(tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len + 1))) {
        tuple<int, int, int> second_prog = H[tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len + 1)];
        int secondOccur = succInProgression(second_prog, i);
        if(secondOccur <= i + v_len) return secondOccur;
    }
    return -1;
}

/*
See Kociumaka 2012, p. 288 for source

Returns the maximal Index j in [i - |v|, i] such that w[j, j + |v| - 1] = v (find last occurence of v in the interval)
if such a j doesnt exist return -1

Time: Avg O(1)
*/
int PRED(int i, pair<int, int> v, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) {
    int v_len = v.second - v.first + 1;
    unsigned int log_len = __bit_width((unsigned int) v.second - v.first + 1) - 1;
    if(H.contains(tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len))) {
        tuple<int, int, int> second_prog = H[tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len)];
        int firstOccur = predInProgression(second_prog, i);
        if(firstOccur != -1 && firstOccur >= i - v_len) {
            return firstOccur;
        }
    }
    if(H.contains(tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len - 1))) {
        tuple<int, int, int> first_prog = H[tuple<int, int, int>(log_len, DBF[log_len][v.first], i / v_len - 1)];
        int secondOccur = predInProgression(first_prog, i);
        if(secondOccur >= i-v_len) return secondOccur;
    }
    return -1;
}
/*
See Kociumaka 2012, p. 291 for source

Construct Hashtable H[length, DBF identifier, setNum] = (start, period, end)  such that
H[i, j, k] = (a, p, b) iff the occurences of j = DBF[i][m] in [setNum, setNum + 2^i] form the arithmetic progression (a, p, b)

Time: Avg O(n log n)
Space: O(n log n)
This is especially used for SUCC / PRED Queries in O(1)
*/
unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> constructH(vector<vector<int>> DBF) { //Avg. Time O(nlogn)
    unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H; // Triplets (Länge, Identifier, SetNUM) und (Erste Pos, Periode, Letzte Pos)
    int pow = 1;
    const unsigned int n = DBF[0].size();
    for(int k = 0; k < __bit_width(n); k++) {
        for(int j = 0; j < DBF[k].size(); j++) {
            int setNum = j / pow;
            tuple<int, int, int> tripletPair = tuple<int, int, int>({k, DBF[k][j], setNum});
            if(H.contains(tripletPair)) { //2^k = len
                tuple<int, int, int> curr = H[tripletPair];
                get<2>(curr) = j; //setze neue letzte occurence
                if(get<1>(curr) == -1) get<1>(curr) = j - get<0>(curr); //Wir haben 2. Vorkommnis gefunden -> Setze Periode
                H[tripletPair] = curr;
            }
            else {
                H[tripletPair] = tuple<int, int, int>({j, -1, j});
            }
        }
        pow *= 2;
    }
    return H;
}
/*
See Kociumaka 2012, p. 288 for source

Given substring x and y with |y| <= 2|x|, find all occurrences of x in y as an arithmetic progression (start, period, end)
*/
tuple<int, int, int> Occ(pair<int, int> x, pair<int, int> y, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) {
    if(y.second < 0) return tuple<int, int, int>{-1, -1, -1};
    y.first = max(0, y.first); //Falls negative Intervall Grenzen angegeben werden
    tuple<int, int, int> res = tuple<int, int, int>{-1, -1, -1}; //start, period, end
    int x_len = x.second - x.first + 1;
    int y_len = y.second - y.first + 1;
    int p = SUCC(y.first, x, H, DBF);
    if(p == -1 || p + x_len - 1> y.second) return res;
    get<0>(res) = p - y.first; //Erstes Vorkommnis
    get<2>(res) = p - y.first; //Letztes Vorkommnis
    int q = SUCC(p+1, x, H, DBF);
    if(q == -1 || q + x_len - 1> y.second) return res;
    get<2>(res) = q - y.first; //Setze Letztes Vorkommnis neu
    get<1>(res) = q - p; //Setze Periode
    int last = PRED(y.second - x_len + 1, x, H, DBF);
    if(last < y.first) return res;
    get<2>(res) = last - y.first; //Neues Letztes Vorkommnis
    return res;
}

/*
See Kociumaka 2012, p. 289 for source

Given substrings x and y with |x| = |y| = 2^k compute all large Prefix Suffixes
formally: Compute the arithmetic progression m = (a, p, b) such that for all r in the progression m
(1) x[0 .. r] = y[|y| - r .. |y|]
(2) r > d = 2^k / 2

To understand why this works look at the source, especially the image on p. 288

Time: Avg. 0(1)
*/
tuple<int, int, int> LargePS(pair<int, int> x, pair<int, int> y, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) { //x, y sind Basic Factors
    unsigned int len = x.second - x.first + 1;
    int log_len = __bit_width(len) - 1;
    if(len == 1) return (tuple<int, int, int>{(DBF[log_len][x.first] == DBF[log_len][y.first] ? 1 : -1), -1, (DBF[log_len][x.first] == DBF[log_len][y.first] ? 1 : -1)});
    //Unterteile x, y in ihre beiden Hälften mit |x1| = |x2| = |y1| = |y2| = d
    int d = len / 2;
    pair<int, int> x1 = {x.first, x.first + (x.second-x.first) / 2};
    pair<int, int> x2 = {x.first + ((x.second-x.first) / 2) + 1, x.second};
    pair<int, int> y2 = {y.first, y.first + (y.second-y.first) / 2};
    pair<int, int> y1 = {y.first + ((y.second-y.first) / 2) + 1, y.second};
    //Berechne linke Progression (Lemma 4)
    tuple<int, int, int> Occ1 = Occ(x1, y, H, DBF);
    int leftshift = 2*d;
    tuple<int, int, int> S1 = tuple<int, int, int>{leftshift - get<2>(Occ1), get<1>(Occ1), leftshift - get<0>(Occ1)}; //wir tauschen zusätzlich start und ende von der Arithmetischne Folgen, damit beide increasing sind
    //Berechne rechte Progression (Lemma 4)
    tuple<int, int, int> Occ2 = Occ(y1, x, H, DBF);
    int rightshift = d;
    tuple<int, int, int> S2 = tuple<int, int, int>{rightshift + get<0>(Occ2), get<1>(Occ2), rightshift + get<2>(Occ2)};

    //Fall |S1| <= 2 oder |S2| <= 2
    if(get<0>(S1) == get<2>(S1)) { //Fall |S1| == 1
        return twoElementsInProgression(S2, get<0>(S1), -1, d);
    }
    else if(get<0>(S1) + get<1>(S1) == get<2>(S1) || get<1>(S1) == -1) { //Fall |S1| == 2
        return twoElementsInProgression(S2, get<0>(S1), get<2>(S1), d);
    }
    else if(get<0>(S2) == get<2>(S2)) { //Fall |S2| == 1
        return twoElementsInProgression(S1, get<0>(S2), -1, d);
    }
    else if(get<0>(S2) + get<1>(S2) == get<2>(S2) || get<1>(S2) == -1) { //Fall |S2| == 2
        return twoElementsInProgression(S1, get<0>(S2), get<2>(S2), d);
    }

    //Necessarily we have now per(S1) = per(S2)
    if(get<1>(S1) != get<1>(S2)) cerr << "Unequal period!" <<endl;
    int period = get<1>(S1);
    //Intersection between two arithmetic progressions in constant time
    if(get<0>(S1) % period != get<0>(S2) % period) return tuple<int, int, int>{-1, -1, -1}; //If the progressions arent exactly synced
    //Nehme nun also an, dass die Progressions genau aufeinander liegen => finde nun max start und min ende
    int maxStart = max(get<0>(S1), get<0>(S2));
    int minEnd = min(get<2>(S1), get<2>(S2));
    if(maxStart < minEnd) {
        if(maxStart == d) return tuple<int, int, int>{maxStart+period, period, minEnd};
        else return tuple<int, int, int>{maxStart, period, minEnd};
    }
    else if(maxStart == minEnd && maxStart != d) {
        return tuple<int, int, int>{maxStart, -1, minEnd};
    }
    else return tuple<int, int, int>{-1, -1, -1};
}

/*
See Kociumaka 2012, p. 290 for source

Given substring u and integer M such that M = largest Power of 2 smaller than |u|, find all Borders of u larger than M (as an arithmetic progression)

Time: Avg. O(1)
*/
tuple<int, int, int> bordersLarger(pair<int, int> u, int M, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) {
    int u_len = u.second - u.first + 1;
    pair<int, int> prefix = {u.first, u.first + M-1};
    int q = SUCC(u.first+1, prefix, H, DBF);
    if(q == -1 || q > u.second) return tuple<int, int, int>{-1, -1, -1};
    int d = q - u.first;
    //check whether d is period
    pair<int, int> suffix = {u.second-M + 1, u.second};
    int e = PRED(u.second-M, suffix, H, DBF);
    if(e == -1) return tuple<int, int, int>{-1, -1, -1};
    if(d == suffix.first - e) return tuple<int, int, int>{M, d, (((u_len-M)/d) * d) + M};
    else return tuple<int, int, int>{-1, -1, -1};
}

/*
See Kociumaka 2012, p. 287 for source

Given substring, return whether it is primitive

Time: Avg. O(1)
*/
bool isPrim(pair<int, int> substr, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) {
    vector<tuple<int, int, int>> Borders;
    int substr_len = substr.second - substr.first + 1;
    if(substr_len == 1) return -1;
    //Compute I (Pairs von Prefix / Suffixes die 2er Potenzen sind von u)
    vector<pair<pair<int, int>, pair<int, int>>> I;
    int pow = highestPowerOfTwoIn(substr_len);
    if(pow == substr_len) I.push_back(make_pair(make_pair(substr.first, substr.first + pow/2 - 1), make_pair(substr.second - pow/2 + 1, substr.second)));
    I.push_back(make_pair(make_pair(substr.first, substr.first + pow - 1), make_pair(substr.second - pow + 1, substr.second)));


    for(pair<pair<int, int>, pair<int, int>> p : I) {
        pair<int, int> xi = p.first;
        pair<int, int> yi = p.second;
        Borders.push_back(LargePS(xi, yi, H, DBF));
    }
    //Borders.push_back(bordersLarger(substr, highestPowerOfTwoIn(substr_len), H, DBF));
    tuple<int, int, int> bl = bordersLarger(substr, highestPowerOfTwoIn(substr_len), H, DBF);
    int maxBorder = -1; //Find largest non-trivial border (smaller than length of string we are considering)
    for(auto b : Borders) { //O(1)
        if(get<0>(b) == substr_len) continue; //Wenn Start bereits triviale Border ist, kann nichts weiter kommen

        if(get<0>(b) == get<2>(b) || get<2>(b) == -1) maxBorder = max(maxBorder, get<0>(b)); //Nur Start betrachten
        else if(get<2>(b) == substr_len) {
            if(get<1>(b) != -1) maxBorder = max(maxBorder, get<2>(b) - get<1>(b));
            else if(get<1>(b) == -1) maxBorder = max(maxBorder, get<0>(b));
        }
        else maxBorder = max(maxBorder, get<2>(b));
    }
    if(get<1>(bl) != -1) return (substr_len % get<1>(bl)) != 0;
    //if(maxBorder == -1) return 1;
    if(maxBorder >= substr_len / 2) {
        int mPer = substr_len - maxBorder;
        return ((substr_len % mPer) != 0);
    }
    return true;
}

bool isPrimAlt(pair<int, int> substr, unordered_map<tuple<int, int, int>, tuple<int, int, int>, boost::hash<tuple<int, int, int>>> H, vector<vector<int>> DBF) {
    vector<tuple<int, int, int>> Borders;
    int substr_len = substr.second - substr.first + 1;
    if(substr_len == 1) return -1;
    int pow = highestPowerOfTwoIn(substr_len);
    tuple<int, int, int> bl = bordersLarger(substr, pow , H, DBF);

    if(get<1>(bl) != -1) {
        //cerr << "BL case" << endl;
        return (substr_len % get<1>(bl)) != 0;
    }
    Borders.push_back(LargePS(make_pair(substr.first, substr.first + pow - 1), make_pair(substr.second - pow + 1, substr.second), H, DBF));
    if(pow == substr_len) Borders.push_back(LargePS(make_pair(substr.first, substr.first + pow/2 - 1), make_pair(substr.second - pow/2 + 1, substr.second), H, DBF));

    int maxBorder = -1; //Find largest non-trivial border (smaller than length of string we are considering)
    for(auto b : Borders) { //O(1)
        if(get<0>(b) == substr_len) continue; //Wenn Start bereits triviale Border ist, kann nichts weiter kommen
        if(get<0>(b) == get<2>(b) || get<2>(b) == -1) maxBorder = max(maxBorder, get<0>(b)); //Nur Start betrachten
        else if(get<2>(b) == substr_len) {
            if(get<1>(b) != -1) maxBorder = max(maxBorder, get<2>(b) - get<1>(b));
            else if(get<1>(b) == -1) maxBorder = max(maxBorder, get<0>(b));
        }
        else maxBorder = max(maxBorder, get<2>(b));
    }
    if(maxBorder >= substr_len / 2) {
        int mPer = substr_len - maxBorder;
        return ((substr_len % mPer) != 0);
    }
    return true;
}
