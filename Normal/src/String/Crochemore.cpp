#include<vector>
#include<string>
#include<set>
#include<list>
using namespace std;
/*
Implementation of Crochemore Algorithm for finding all (maximum) repetitions of primitive subword in a word
See (Crochemore, 1981) and (Crochemore, Hancart, Lecroq, 2007) for references
Runtime: O(nlogn)
Computes: Triples (i, p, e) with i = start position, p = period, e = power
Output: Set P with P[j] = {u | u primitive s.t. u^2 suffix of w[1..j]}
*/
vector<vector<int>> crochemoreP(string w) {
    vector<vector<int>> repetitions;
    set<char> alph;
    for(char c : w) alph.insert(c); //O(nlogn) runtime
    int alph_size = alph.size();
    vector<int> rank(27);
    int curr_rank = 0;
    for(auto it = alph.begin(); it != alph.end(); ++it) {
        rank[*it - 'a'] = curr_rank;
        curr_rank++;
    }
    const int n = w.length();
    vector<list<int>> C(alph_size);
    vector<int> E(n);
    vector<list<int>::iterator> E_ref(n);
    for(int i = 0; i < n; i++) {
        C[rank[w[i] - 'a']].push_back(i);
        E[i] = rank[w[i]-'a'];
        E_ref[i] = C[rank[w[i]-'a']].end();
        E_ref[i]--;
    }

    vector<list<int>> small;
    vector<int> D(n);
    vector<list<int>> DCLASS(n+1);
    vector<list<int>::iterator> D_ref(n);

    for(int i = 0; i < C.size(); i++) {
        small.push_back(C[i]);
        auto last = C[i].end();
        last--;
        for(auto it = C[i].begin(); it != last; ++it) {
            auto next = it;
            ++next;
            D[*it] = *next - *it;
            DCLASS[D[*it]].push_back(*it);
            D_ref[*it] = DCLASS[D[*it]].end();
            D_ref[*it]--;
        }
        D[C[i].back()] = 0; //0 = INF
        DCLASS[0].push_back(C[i].back());
        D_ref[C[i].back()] = DCLASS[0].end(); D_ref[C[i].back()]--;
    }
    int k = 1;

    while(!small.empty()) {
        //Compute Repetitions with Period k
        while(!DCLASS[k].empty()) {
            int i = *(DCLASS[k].begin());
            do i +=k; while(D[i] == k && i < n);
            int e = 1;
            do {
                i -= k;
                e++;
                DCLASS[k].erase(D_ref[i]);
                D_ref[i] = DCLASS[k].end();
                if(i-k < 0 || D[i-k] != k) repetitions.push_back({i, k, e}); //If repetition is maximal, store it
            } while(i-k >= 0 && D[i-k] == k);
        }

        //Compute new Subclasses
        vector<int> consideredClassesIndex;
        vector<bool> consideredClasses(n);
        vector<vector<list<int>>> subclasses(C.size());
        for(list<int> P : small) {
            vector<list<int>> C_twin(C.size());
            vector<bool> consideredPairs(n);
            vector<int> consideredPairsIndex;
            for(int i : P) {
                if(i == 0) continue;
                int c_index = E[i-1];
                if(E_ref[i-1] != C[c_index].begin()) { //If i-1 has predecessor
                    auto pred = E_ref[i-1]; --pred; //fix that predecessor
                    //Since the predecessor now gets a new D-Value:
                    if(D_ref[*pred] != DCLASS[D[*pred]].end()) DCLASS[D[*pred]].erase(D_ref[*pred]); //Remove it from its own class
                    D[*pred] = (D[i-1] == 0 ? 0 : D[*pred] + D[i-1]); //Update its D-Value
                    DCLASS[D[*pred]].push_back(*pred); //Add it to its new D-Class
                    D_ref[*pred] = DCLASS[D[*pred]].end(); //Adjust pointer
                    D_ref[*pred]--;
                }
                C[c_index].erase(E_ref[i-1]); //Remove i-1 from old class

                C_twin[c_index].push_back(i-1); //And add it to the twin-class
                if(D_ref[i-1] != DCLASS[D[i-1]].end()) DCLASS[D[i-1]].erase(D_ref[i-1]); //i-1 doesnt have a successor, thus its D-Val is Infinite
                D[i-1] = 0; //Infinity is coded as the integer 0
                DCLASS[0].push_back(i-1);
                D_ref[i-1] = DCLASS[0].end(); D_ref[i-1]--;
                if(C_twin[c_index].size() > 1) { //If i-1 has predecessor
                    auto pred = C_twin[c_index].end(); --(--pred); //Check the second to last Element.
                    //This Element now has a larger gap since i-1 was removed
                    //Thus, update D, Dclass and D_ref analogously to above
                    if(D_ref[*pred] != DCLASS[D[*pred]].end()) DCLASS[D[*pred]].erase(D_ref[*pred]);
                    D[*pred] = (i-1) - *pred; //New D-Value is the distance to the last element
                    DCLASS[D[*pred]].push_back(*pred);
                    D_ref[*pred] = DCLASS[D[*pred]].end();
                    D_ref[*pred]--;
                }
                if(!consideredPairs[c_index]) consideredPairsIndex.push_back(c_index);
                consideredPairs[c_index] = true;
            }
            for(int j : consideredPairsIndex) {
                subclasses[j].push_back(C_twin[j]);
                if(!consideredClasses[j]) consideredClassesIndex.push_back(j);
                consideredClasses[j] = true;
            }
        }
        //Choice of Small classes
        small.clear();
        for(int j : consideredClassesIndex) {
            if(!C[j].empty()) subclasses[j].push_back(C[j]);
            //Replace C by its Subclasses
            C[j] = subclasses[j][0];
            for(auto it = C[j].begin(); it != C[j].end(); ++it) {
                E[*it] = j;
                E_ref[*it] = it;
            }
            int maxClass = 0;

            for(int m = 1; m < subclasses[j].size(); m++) {
                if(subclasses[j][m].size() > subclasses[j][maxClass].size()){
                    small.push_back(subclasses[j][maxClass]);
                    maxClass = m;
                }
                else small.push_back(subclasses[j][m]);

                C.push_back(subclasses[j][m]);
                int lastIndex = C.size()-1;
                for(auto it = C[lastIndex].begin(); it != C[lastIndex].end(); ++it) {
                    E[*it] = lastIndex;
                    E_ref[*it] = it;
                }
            }
        }
        k++;
    }
    vector<vector<int>> P(n+1);
    //In each iteration of the inner loop, we find one new element of P
    //-> Since |P| = O(nlogn) the runtime will also be log-linear
    for(vector<int> rep : repetitions) {
        int start = rep[0];
        int period = rep[1];
        int exp = rep[2];
        int pos = start+2*period;
        for(int i = 0; i < exp-1; i++) {
            P[pos].push_back(period); //O(1)
            pos += period; //O(1)
        }
    }
    return P;
}
