#ifndef drawGraphReg_H
#define drawGraphReg_H
#include <string>
#include <vector>
#include <optional>
int visualizeGraphReg(std::string w, std::string pat, std::vector<std::vector<int>> nodes, std::vector<std::vector<int>> edges);
#endif
