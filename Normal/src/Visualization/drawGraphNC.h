#ifndef drawGraphNC_H
#define drawGraphNC_H
#include <string>
#include <vector>
#include <optional>
int visualizeGraphNC(std::string w, std::string pat, std::vector<std::vector<int>> nodes, std::vector<std::vector<std::vector< std::tuple<int, int, int, std::optional<std::pair<int, int>>> >>> edges, std::vector<std::vector<int>> progressionIndices);
#endif
