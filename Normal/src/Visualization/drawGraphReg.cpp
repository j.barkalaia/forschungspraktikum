#include "raylib.h"
#include <string>
#include <vector>
using namespace std;


/*
This file contains a simple method which visualizes graphs given by the regular instance, using raylib.
*/

int visualizeGraphReg(string w, string pat, vector<vector<int>> nodes, vector<vector<int>> edges) {
    const int screenWidth = 1000;
    const int screenHeight = 800;

    string t = "pat = " + pat + " w = " + w;
    InitWindow(screenWidth, screenHeight, t.c_str());

    SetTargetFPS(60);

    while (!WindowShouldClose())    // Detect window close button or ESC key
    {

        BeginDrawing();

            ClearBackground(RAYWHITE);

            const int s = nodes.size();
            const int widthOffset = screenWidth/10;
            //const int heightOffset = screenHeight/15;
            const int nodeRadius = 20;
            const int fontSize = 20;
            for(int i = 0; i < s; i++) {
                int n = nodes[i].size();
                int heightOffset = screenHeight/(2*n);
                //Draw Nodes
                for(int j = 0; j < n; j++) {
                    int x = screenWidth/s * i + widthOffset;
                    int y = screenHeight/n * j +heightOffset;
                    string temp = std::to_string(nodes[i][j]);
                    const char* text = temp.c_str();
                    DrawCircleLines(x, y, nodeRadius, RED);
                    DrawText(text, x - fontSize/4, y - fontSize/2, fontSize, GREEN);
                }
                //Draw Edges
                if(i < s-1) {
                    int next_n = nodes[i+1].size();
                    int next_heightOffset = screenHeight/(2*next_n);
                    for(int j = 0; j < n; j++) {
                        int node_x = screenWidth/s * i + widthOffset + nodeRadius;
                        int node_y = screenHeight/n * j +heightOffset;
                        int target_node_x = screenWidth/s * (i+1) + widthOffset - nodeRadius;
                        int target_node_y = screenHeight/next_n * edges[i][j] +next_heightOffset;

                        DrawLine(node_x, node_y, target_node_x, target_node_y, PURPLE);
                    }
                }
            }

        EndDrawing();
    }
    CloseWindow();
    return 0;
}
