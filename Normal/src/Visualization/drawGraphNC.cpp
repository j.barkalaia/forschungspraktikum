#include "raylib.h"
#include <string>
#include "../Pattern/noncrossMatching.h"

using namespace std;

/*
This file contains a simple method which visualizes graphs given by the non-cross instance, using raylib.
*/

int visualizeGraphNC(string w, string pat, vector<vector<int>> nodes, vector<vector<vector<arithmProg>>> edges, vector<vector<int>> progressionIndices) {

    string t = "pat = " + pat + " w = " + w;

    const int screenWidth = 1000;
    const int screenHeight = 800;
    SetTargetFPS(60);
    InitWindow(screenWidth, screenHeight, t.c_str());
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {

        BeginDrawing();

            ClearBackground(RAYWHITE);

            const int s = nodes.size();
            const int widthOffset = screenWidth/10;
            const int nodeRadius = 20;
            const int fontSize = 20;

            for(int i = 0; i < s; i++) {
                int n = nodes[i].size();
                int heightOffset = screenHeight/(2*n);
                //Draw Nodes
                for(int j = 0; j < n; j++) {
                    int x = screenWidth/s * i + widthOffset;
                    int y = screenHeight/n * j +heightOffset;
                    string temp = std::to_string(nodes[i][j]);
                    const char* text = temp.c_str();
                    DrawCircleLines(x, y, nodeRadius, RED);
                    DrawText(text, x - fontSize/4, y - fontSize/2, fontSize, GREEN);
                }
                //Draw Edges
                if(i < s-1) {
                    int next_n = nodes[i+1].size();
                    int next_heightOffset = screenHeight/(2*next_n);
                    for(int j = 0; j < n; j++) {
                        int node_x = screenWidth/s * i + widthOffset + nodeRadius;
                        int node_y = screenHeight/n * j +heightOffset;
                        for(arithmProg edge : edges[i][j]) {
                            auto [start, period, end, r] = edge;

                            int target_node_x = screenWidth/s * (i+1) + widthOffset - nodeRadius;
                            if(r.has_value()) {
                                auto [progIndex, progPosIndex] = r.value();
                                vector<int> currProg = progressionIndices[progIndex];

                                for(int k = progPosIndex; k < currProg.size(); k++) {
                                    int target_node_y = screenHeight/next_n * currProg[k] +next_heightOffset;
                                    DrawLine(node_x, node_y, target_node_x, target_node_y, RED);
                                }
                            }
                            else {
                                int target_node_y = screenHeight/next_n * start +next_heightOffset;
                                DrawLine(node_x, node_y, target_node_x, target_node_y, PURPLE);
                                if(period == 1) {
                                    for(int y = start+1; y < next_n; y += period) {
                                        target_node_y = screenHeight/next_n * y +next_heightOffset;
                                        DrawLine(node_x, node_y, target_node_x, target_node_y, BLACK);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        EndDrawing();
    }
    CloseWindow();

    return 0;
}
