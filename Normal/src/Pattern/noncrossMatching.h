#ifndef noncrossMatching_H
#define noncrossMatching_H
#include<vector>
#include<string>
#include<optional>
#include<list>
typedef std::tuple<int, int, int, int> doubleSideProgressionEdge; //starting node, period, end, offset
typedef std::tuple<int, int, int, std::optional< std::pair< int, int > >  > arithmProg;
std::pair<std::tuple<std::vector<std::vector<int>>, std::vector<std::vector<std::vector<arithmProg>>>, std::vector<std::vector<int>>>, int> nonCrossGraph(std::string w, std::string pat);
bool nonCrossDecision(std::string w, std::string pat);
int numSolutionsNC(std::string w, std::string pat, std::vector<std::pair<int, int>> variables, std::vector<std::list<doubleSideProgressionEdge>> progressions);
std::vector<std::vector<std::pair<int, int>>> walkNextNode(std::vector<std::vector<int>> nodes, std::vector<std::vector<std::vector<arithmProg>>> edges, std::vector<std::vector<int>> progressionIndices);
#endif
