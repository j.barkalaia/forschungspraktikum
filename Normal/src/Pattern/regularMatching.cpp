#include<iostream>
#include<vector>
#include"patternDSA.h"
#include"../String/stringDSA.h"
using namespace std;

/*
This file contains the algorithms for the regular matching problem.
*/


/*
Finds nextBranch as detailed in the paper (for regular patterns).
Very self explanatory - simply check for each node whether it has multiple outgoing edges,
i.e. whether its edge of period 1 does not point to the last node of the next level.
*/
vector<vector<pair<int, int>>> walkNextNode(vector<vector<int>> occurMatrix, vector<vector<int>> maxMatchMatrix) {
    vector<vector<pair<int, int>>> nextBranch = vector<vector<pair<int, int>>>(occurMatrix.size());
    nextBranch[occurMatrix.size()-1] = vector<pair<int, int>>((occurMatrix.back()).size(), make_pair(-1, -1));
    for(int i = occurMatrix.size() - 2; i >= 0; i--) {
        for(int j = 0; j < occurMatrix[i].size(); j++) {
            if(maxMatchMatrix[i][j] < occurMatrix[i+1].size() - 1) {
                nextBranch[i].push_back(make_pair(i, j));
            }
            else {
                nextBranch[i].push_back(nextBranch[i+1][maxMatchMatrix[i][j]]);
            }
        }
    }
    return nextBranch;
}

/*
Find the edges of the graph G' as detailed in the paper by merging every two following levels of nodes.
*/
pair<vector<vector<int>>,vector<vector<int>>> calcDefaultEdges(vector<vector<int>> occurMatrix, vector<string> words) {
    vector<vector<int>> maxMatchMatrix;
    maxMatchMatrix.push_back({0});
    for(int i = 1; i<occurMatrix.size() - 1; i++) {
        int ptr = 0;
        vector<int> maxMatch;
        for(int j : occurMatrix[i]) {
            while(j + words[i-1].length() >= occurMatrix[i+1][ptr] && ptr < occurMatrix[i+1].size()) {
                ptr++;
            }

            if(maxMatch.empty()) { //.. then j = min(occurMatrix[i])
                if(ptr > 0) occurMatrix[i+1].erase(occurMatrix[i+1].begin(), occurMatrix[i+1].begin()+ ptr); //Delete unreachable nodes
                ptr = 0;
            }

            if(ptr == occurMatrix[i+1].size()) {
                for(int k = 0; k <= occurMatrix[i].size() - ptr; k++) occurMatrix[i].pop_back(); //Delete nodes that dont have a further walk
                break;
            }
            maxMatch.push_back(ptr);
        }
        maxMatchMatrix.push_back(maxMatch);
    }
    return make_pair(occurMatrix, maxMatchMatrix);
}

/*
Find initial nodes of G' i.e. all occurences of the words
*/
vector<vector<int>> calcOccurenceMatrix(string w, int m, vector<string> words) {
    int s = 1;
    int j = words[0].length() + 1;
    vector<vector<int>> occurMatrix;
    occurMatrix.push_back({0});
    while(s <= m-1) {
        auto [positions, last] = findAllOccurences(w, words[s], j);
        j = last + 1;
        if(positions.size() == 0) cerr << "Variable " << s << " unmatchable!" << endl;
        occurMatrix.push_back(positions);
        s++;
    }
    return occurMatrix;
}

pair<vector<vector<int>>, vector<vector<int>>> returnRegGraph(string w, string pat) {
    const int n = w.length();
    auto [variables, words] = factoriseNaive(pat);

    vector<vector<int>> occurMatrix = calcOccurenceMatrix(w, variables.size(), words);
    auto [occurMatrixUpdate, maxMatchMatrix] = calcDefaultEdges(occurMatrix, words);
    return make_pair(occurMatrixUpdate, maxMatchMatrix);
}

/*
Decision algorithm for regular patterns as detailed in the thesis.
*/
bool regDecision(string w, string pat) {
    const int n = w.length();
    auto [variables, words] = factoriseNaive(pat);
    if(w.substr(0, words[0].length()) != words[0]) return false;
    int j = words[0].length();
    int i = 0;
    for(int s  = 1; s < words.size() - 1; s++) {
        i = w.find(words[s], j+1);
        if(i == string::npos) return false;
        j = i + words[s].length();
    }
    return w.substr(n - words.back().length(), words.back().length()) == words.back();
}

/*
Returns number of solutions for regular matching problem by counting the number of
walks of maximal length in the graph G'
*/
int numSolutionsReg(string w, string pat) {
    auto [nodes, edges] = returnRegGraph(w, pat);
    vector<vector<int>> numSolNode = nodes;
    for(int i = 0; i < numSolNode.back().size(); i++)  numSolNode.back()[i] = 1;
    for(int i = numSolNode.size() - 2; i >= 0; i--) {
        int sumPtr = nodes[i+1].size()-1;
        int resPtr = nodes[i].size()-1;
        int currSum = numSolNode[i+1][sumPtr];
        while(resPtr >= 0) {
            while(sumPtr > edges[i][resPtr]) {
                sumPtr--;
                currSum += numSolNode[i+1][sumPtr];
            }
            numSolNode[i][resPtr] = currSum;
            resPtr--;
        }
    }
    long long finalSum = 0;
    for(int s : numSolNode[0]) finalSum += s;
    return finalSum;
}
