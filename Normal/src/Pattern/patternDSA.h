#ifndef patternDSA_H
#define patternDSA_H
#include <vector>
#include <string>
#include "../String/stringDSA.h"
std::pair<std::vector<std::pair<int, int>>, std::vector<std::pair<int, int>>> factorisePattern(std::string pat);
std::pair<std::vector<int>, std::vector<std::string>> factoriseNaive(std::string pat);
std::vector<std::vector<int>> generateR(std::string w, std::string v);
std::vector<std::vector<std::pair<int, int>>> decomposeVariableBlocks(std::string pat, std::vector<std::pair<int, int>> variables);
std::vector<std::vector<std::vector<int>>> calcR(std::vector<std::pair<int, int>> variables, std::vector<std::vector<std::pair<int, int>>> varDecomposed, std::string w, std::string pat);
std::vector<std::vector<int>> constructP(std::string w);
int checkSubstitutionSuffix(std::string pat, std::string w, int j, std::vector<std::tuple<int, int, int>> substitutions, std::vector<std::pair<int, int>> varDecomposition);
int checkVariableBlockPeriodic(std::vector<std::pair<int, int>> varBlockDecomposed, std::pair<int, int> t, std::pair<int, int> ts, std::pair<int, int> v, const int n, lcpStructures lcpStruct);
int uniquePeriodicMatching(const int b, std::vector<std::pair<int, int>> varBlockDecomposed, std::pair<int, int> t, std::pair<int, int> ts, int j, std::pair<int, int> v, std::pair<int, int> vi, const int n, lcpStructures lcpStruct, std::string w, std::string pat);
#endif
